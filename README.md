![Logo](./figs/RATLogo.png)
# Math
<em>Parsing math strings you say, ehh why?</em>

## Introduction
It turns out it is difficult to find a math string parser that can both check units and evaluate complex functions, although there are many available [1,2,3]. This library is needed in Rat to allow model inputs to be equation strings instead of single values. This allows for implementing global variables and makes the need for a separate unit table obsolete. To have the capabilities we need we have implemented our own library, using inspiration from the provided sources.

## Installation
As this library is part of a collection of inter-dependent libraries a detailed description of the installation process is provided in a separate repository located here: [rat-docs](https://gitlab.com/Project-Rat/rat-documentation). It is required that you install the [rat-common](https://gitlab.com/Project-Rat/rat-common) before installing this. 
```bash
mkdir release
cd release
cmake -DCMAKE_BUILD_TYPE=T ..
make -jX
```

where X is the number of cores you wish to use for the build. When the library is build run unit tests and install using

```bash
make test
sudo make install
```

## Example Usage
The library comes with a basic parse function so that it can be run on the command line. Off course the library can also be integrated into your code. Here is a series of basic examples showcasing the capabilities.


### Simple arithmetic
The parser can off course handle scalars and basic operators and operators are applied in the correct order:
```bash
evaluate "(10 + 5)/3 - 4"
```
outputs:
```bash
1
```

### Units
You can add units (square brackets are optional, but often useful):
```bash
evaluate "1[kg*m^3] + 5[kg]*0.2[m^3]"
```
outputs:
```bash
`2 [kg m^3]`. 
```

Square brackets are applied to the numeric value that comes before:
```bash
evaluate "2[kg]^2"
```
outputs:
```bash
4 [kg^2]
```
and not:
```bash
2 [kg^2]
```

### Lorentz Force
Calculating the force on a superconducting cable:
```bash
evaluate "10[T]*8[kA]*0.5[m]"
```
outputs:
```bash
40000 [N]
```

### Ohms Law
Calculating the voltage drop over a resistor:
```bash
evaluate "10[Ohm]*20[mA]"
```
outputs:
```bash
0.2 [V]
```

### Capacitor
Stored energy in a capacitor:
```bash
evaluate "0.5*1[uF]*20[V]^2"
```
outputs:
```bash
0.0002 [J]
```

### Inductor
Stored energy in an inductor:
```bash
evaluate "0.5*50[mH]*8[kA]^2"
```
outputs:
```bash
1600000 [J]
```

### Trigonometric Functions
Many trigonometric functions are available. For example:
```bash
evaluate "10[mm]*sin(pi[rad]/4)"
```
outputs:
```bash
0.007071 [m]
```

### Multi-Argument Functions
Min and maximum:
```bash
evaluate "max(10[m]/2[s], 20[m]/3[s])"
```
outputs:
```bash
6.666667 [m s^-1]
```

and more ....

## Authors
* Jeroen van Nugteren
* Nikkie Deelen

## License
This project is licensed under the [MIT](LICENSE) license.

## References
[1] Andrea Giammarchi, [uparser](https://github.com/WebReflection/uparser), visited 11 jan 2024

[2] Joel de Guzman, Hartmut Kaiser, [boost spirit](https://www.boost.org/doc/libs/1_78_0/libs/spirit/doc/html/index.html), visited 11 jan 2024

[2] Nick McDonald, [dima](https://github.com/weigert/dima), visited 11 jan 2024


