// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// header
#include "astnode.hh"

// general headers
#include <sstream>

// rat common headers
#include "rat/common/error.hh"
#include "rat/common/extra.hh"

// rat specific
namespace rat{namespace math{

	
	ASTNode::ASTNode(){

	}

	ASTNode::ASTNode(const std::string& token){
		token_ = token;
	}

	ShASTNodePr ASTNode::create(
		const std::string& token){
		return std::make_shared<ASTNode>(token);
	}

	ShASTNodePr ASTNode::calculate_derivative(
		const std::string& towards_token, 
		const arma::uword /*depth*/)const{
		if(token_==towards_token){
			return ASTNode::create("1");
		}else{
			return ASTNode::create("0");
		}
	}

	const std::string& ASTNode::get_token()const{
		return token_;
	}

	std::list<std::string> ASTNode::to_rpn_notation()const{
		return {token_};
	}

	ShASTNodePr ASTNode::simplify(const arma::uword /*depth*/)const{
		return ASTNode::create(token_);
	}

	bool ASTNode::is_zero()const{
		return token_ == "0"; 
	}

	bool ASTNode::is_one()const{
		return token_ == "1";
	}

	bool ASTNode::is_equal(const ShASTNodePr& other)const{
		if(other==NULL)return false;
		return token_ == other->token_;
	}

	bool ASTNode::is_number()const{
		return !token_.empty() && (isdigit(token_[0]) || token_[0] == '.');
	}

	std::string ASTNode::as_string()const{
		return token_;
	}

	



	ASTOpNode::ASTOpNode(
		const std::string& token, 
		const ShASTNodePr& left, 
		const ShASTNodePr& right) : ASTNode(token){
		left_ = left;
		right_ = right;
	}

	ShASTOpNodePr ASTOpNode::create(
		const std::string& token, 
		const ShASTNodePr& left, 
		const ShASTNodePr& right){
		return std::make_shared<ASTOpNode>(token, left, right);
	}

	ShASTNodePr ASTOpNode::calculate_derivative(
		const std::string& towards_token,
		const arma::uword depth)const{
		// calculate derivative
		const ShASTNodePr left_derivative = left_->calculate_derivative(towards_token,depth+1);
		const ShASTNodePr right_derivative = right_->calculate_derivative(towards_token,depth+1);

		// check operator
		if(token_=="+" || token_=="-")
			return ASTOpNode::create(
				token_,left_derivative,right_derivative);

		else if(token_=="*" || token_=="$")
			if(left_derivative->is_zero() && right_derivative->is_zero())
				return ASTNode::create("0");
			else if(left_derivative->is_zero())
				return ASTOpNode::create("*",left_,right_derivative);
			else if(right_derivative->is_zero())
				return ASTOpNode::create("*",left_derivative,right_);
			else return ASTOpNode::create("+", 
				ASTOpNode::create("*",left_derivative,right_), 
				ASTOpNode::create("*",left_,right_derivative));
		
		else if(token_=="/")
			if(left_derivative->is_zero() && right_derivative->is_zero())
				return ASTNode::create("0");
			else if(left_derivative->is_zero())
				return ASTOpNode::create("/",
					ASTFnNode::create("neg",ASTOpNode::create("*",left_,right_derivative)),
					ASTOpNode::create("^",right_,ASTNode::create("2")));
			else if(right_derivative->is_zero())
				return ASTOpNode::create("/",
					ASTOpNode::create("*",left_derivative,right_),
					ASTOpNode::create("^",right_,ASTNode::create("2")));
			else
				return ASTOpNode::create("/",ASTOpNode::create("-",
					ASTOpNode::create("*",left_derivative,right_),
					ASTOpNode::create("*",left_,right_derivative)),
					ASTOpNode::create("^",right_,ASTNode::create("2")));

		else if(token_=="^")
			return ASTOpNode::create(
				"*", left_derivative, 
				ASTOpNode::create("*", right_, 
				ASTOpNode::create("^", left_, 
				ASTOpNode::create("-", right_, ASTNode::create("1")))));

		else rat_throw_line("operator not recognized");
	}

	std::list<std::string> ASTOpNode::to_rpn_notation()const{
		std::list<std::string> rpn;
		rpn.splice(rpn.end(), left_->to_rpn_notation());
		rpn.splice(rpn.end(), right_->to_rpn_notation());
		rpn.push_back(token_);
		return rpn;
	}

	ShASTNodePr ASTOpNode::simplify(const arma::uword depth)const{
		// children
		const ShASTNodePr simplify_left = left_->simplify(depth+1);
		const ShASTNodePr simplify_right = right_->simplify(depth+1);

		// addition and subtraction
		if(token_=="+" || token_=="-"){
			if(simplify_left->is_zero() && simplify_right->is_zero()){
				return ASTNode::create("0");
			}

			if(simplify_left->is_zero() && !simplify_right->is_zero()){
				if(token_=="-")return ASTFnNode::create("neg",simplify_right);
				if(token_=="+")return simplify_right;
			}

			if(!simplify_left->is_zero() && simplify_right->is_zero()){
				return simplify_left;
			}

			if(simplify_left->is_number() && simplify_right->get_token()=="/"){
				const ShASTOpNodePr opnode_right = std::dynamic_pointer_cast<ASTOpNode>(simplify_right);
				if(opnode_right==NULL)rat_throw_line("is not an operator node");
				if(opnode_right->left_->is_number() && opnode_right->right_->is_number()){
					return ASTOpNode::create("/",ASTOpNode::create(token_, 
						ASTOpNode::create("*", simplify_left, opnode_right->right_), 
						opnode_right->left_), opnode_right->right_);
				}
			}

			if(simplify_left->get_token()=="/" && simplify_right->is_number()){
				const ShASTOpNodePr opnode_left = std::dynamic_pointer_cast<ASTOpNode>(simplify_left);
				if(opnode_left==NULL)rat_throw_line("is not an operator node");
				if(opnode_left->left_->is_number() && opnode_left->right_->is_number()){
					return ASTOpNode::create("/",ASTOpNode::create(token_,opnode_left->left_,
						ASTOpNode::create("*",simplify_right,opnode_left->right_)),opnode_left->right_);
				}
			}

			if(simplify_left->get_token()=="/" && simplify_right->get_token()=="/"){
				const ShASTOpNodePr opnode_left = std::dynamic_pointer_cast<ASTOpNode>(simplify_left);
				const ShASTOpNodePr opnode_right = std::dynamic_pointer_cast<ASTOpNode>(simplify_right);
				if(opnode_right==NULL)rat_throw_line("is not an operator node");
				if(opnode_left==NULL)rat_throw_line("is not an operator node");
				if(opnode_right->left_->is_number() && opnode_right->right_->is_number() && 
					opnode_left->left_->is_number() && opnode_left->right_->is_number()){
					return ASTOpNode::create("/",ASTOpNode::create(token_,
						ASTOpNode::create("*",opnode_left->left_,opnode_right->right_),
						ASTOpNode::create("*",opnode_right->left_,opnode_left->right_)),
						ASTOpNode::create("*",opnode_left->right_,opnode_right->right_));
				}
			}

			if(simplify_left->is_number() && simplify_right->is_number()){
				// get a and b
				const fltp a = std::stof(simplify_left->get_token());
				const fltp b = std::stof(simplify_right->get_token());
				const fltp c = token_=="-" ? a-b : a+b;

				// create string
				std::stringstream ss; ss<<std::abs(c);
				
				// const bool is_integer = std::abs(c - std::round(c))<1024*arma::Datum<fltp>::eps;
				// const std::string str = is_integer ? std::to_string(int(std::abs(c))) : std::to_string(std::abs(c));
				if(c<0)return ASTFnNode::create("neg",ASTNode::create(ss.str())); else return ASTNode::create(ss.str());
			}

			if(simplify_left->is_equal(simplify_right)){
				if(token_=="-")return ASTNode::create("0");
				if(token_=="+")return ASTOpNode::create("*",ASTNode::create("2"),simplify_right);
			}

			if(simplify_right->get_token()=="neg"){
				const ShASTFnNodePr fnnode = std::dynamic_pointer_cast<ASTFnNode>(simplify_right);
				if(fnnode==NULL)rat_throw_line("is not a function node");
				if(token_=="+")return ASTOpNode::create("-",simplify_left,fnnode->get_operand());
				if(token_=="-")return ASTOpNode::create("+",simplify_left,fnnode->get_operand());
			}

			if(simplify_left->get_token()=="neg"){
				// std::cout<<left_->as_string()<<" "<<token_<<" "<<right_->as_string()<<std::endl;
				const ShASTFnNodePr fnnode = std::dynamic_pointer_cast<ASTFnNode>(simplify_left);
				if(fnnode==NULL)rat_throw_line("is not a function node");
				if(token_=="+")return ASTOpNode::create("-",simplify_right,fnnode->get_operand());
				if(token_=="-")return ASTFnNode::create("neg",ASTOpNode::create("+",fnnode->get_operand(),simplify_right));
			}
		}

		// precedence is already determined
		else if(token_=="$"){
			return ASTOpNode::create("*",simplify_left,simplify_right)->simplify();
		}

		// multiplication
		else if(token_=="*"){
			
			if(simplify_left->is_zero() || simplify_right->is_zero()){
				return ASTNode::create("0");
			}

			if(simplify_left->is_equal(simplify_right)){
				return ASTOpNode::create("^",simplify_left,ASTNode::create("2"));
			}

			if(simplify_left->is_one()){
				return simplify_right;
			}

			if(simplify_right->is_one()){
				return simplify_left;
			}

			if(simplify_left->get_token()=="/" && simplify_right->get_token()=="/"){
				const ShASTOpNodePr opnode_left = std::dynamic_pointer_cast<ASTOpNode>(simplify_left);
				const ShASTOpNodePr opnode_right = std::dynamic_pointer_cast<ASTOpNode>(simplify_right);
				if(opnode_left==NULL)rat_throw_line("is not an operator node");
				if(opnode_right==NULL)rat_throw_line("is not an operator node");
				if(opnode_right->left_->is_number() && opnode_right->right_->is_number() && 
					opnode_left->left_->is_number() && opnode_left->right_->is_number()){
					return ASTOpNode::create("/", 
						ASTOpNode::create("*",opnode_left->left_,opnode_right->left_), 
						ASTOpNode::create("*",opnode_left->right_,opnode_right->right_));
				}
			}

			// move number to left
			if(!simplify_left->is_number() && simplify_right->is_number()){
				ASTOpNode::create("*",simplify_right,simplify_left);
			}

			if(simplify_left->is_number() && simplify_right->is_number()){
				const fltp a = std::stof(simplify_left->get_token());
				const fltp b = std::stof(simplify_right->get_token());
				const fltp c = a*b;
				std::stringstream ss; ss<<c;
				return ASTNode::create(ss.str());
			}

			if(simplify_left->get_token()=="+" || simplify_left->get_token()=="-"){
				const ShASTOpNodePr opnode_left = std::dynamic_pointer_cast<ASTOpNode>(simplify_left);
				if(opnode_left==NULL)rat_throw_line("is not an operator node");
				return ASTOpNode::create(simplify_left->get_token(),ASTOpNode::create("*",simplify_right,opnode_left->left_),ASTOpNode::create("*",simplify_right,opnode_left->right_));
			}

			if(simplify_right->get_token()=="+" || simplify_right->get_token()=="-"){
				const ShASTOpNodePr opnode_right = std::dynamic_pointer_cast<ASTOpNode>(simplify_right);
				if(opnode_right==NULL)rat_throw_line("is not an operator node");
				return ASTOpNode::create(simplify_right->get_token(),ASTOpNode::create("*",simplify_left,opnode_right->left_),ASTOpNode::create("*",simplify_left,opnode_right->right_));
			}

			if(simplify_left->get_token()=="^" && simplify_right->get_token()=="^"){
				const ShASTOpNodePr opnode_left = std::dynamic_pointer_cast<ASTOpNode>(simplify_left);
				const ShASTOpNodePr opnode_right = std::dynamic_pointer_cast<ASTOpNode>(simplify_right);
				if(opnode_left==NULL)rat_throw_line("is not an operator node");
				if(opnode_right==NULL)rat_throw_line("is not an operator node");
				if(opnode_left->left_->is_equal(opnode_right->left_)){
					return ASTOpNode::create("^",opnode_left->left_,ASTOpNode::create("+",opnode_left->right_,opnode_right->right_));
				}
			}

			if(simplify_left->get_token()!="^" && simplify_right->get_token()=="^"){
				const ShASTOpNodePr opnode_right = std::dynamic_pointer_cast<ASTOpNode>(simplify_right);
				if(opnode_right==NULL)rat_throw_line("is not an operator node");
				if(opnode_right->left_->is_equal(simplify_left)){
					return ASTOpNode::create("^",opnode_right->left_,ASTOpNode::create("+",opnode_right->right_,ASTNode::create("1")));
				}
			}

			if(simplify_left->get_token()=="^" && simplify_right->get_token()!="^"){
				const ShASTOpNodePr opnode_left = std::dynamic_pointer_cast<ASTOpNode>(simplify_left);
				if(opnode_left==NULL)rat_throw_line("is not an operator node");			
				if(opnode_left->left_->is_equal(simplify_right)){
					return ASTOpNode::create("^",opnode_left->left_,ASTOpNode::create("+",opnode_left->right_,ASTNode::create("1")));
				}
			}

			if(simplify_right->get_token()=="neg"){
				const ShASTFnNodePr fnnode = std::dynamic_pointer_cast<ASTFnNode>(simplify_right);
				if(fnnode==NULL)rat_throw_line("is not a function node");
				return ASTFnNode::create("neg",ASTOpNode::create("*",simplify_left,fnnode->get_operand()));
			}

			if(simplify_left->get_token()=="neg"){
				const ShASTFnNodePr fnnode = std::dynamic_pointer_cast<ASTFnNode>(simplify_left);
				if(fnnode==NULL)rat_throw_line("is not a function node");
				return ASTFnNode::create("neg",ASTOpNode::create("*",simplify_right,fnnode->get_operand()));
			}

			if(simplify_left->get_token()=="*" || simplify_left->get_token()=="/"){
				const ShASTOpNodePr opnode_left = std::dynamic_pointer_cast<ASTOpNode>(simplify_left);
				if(opnode_left==NULL)rat_throw_line("is not an operator node");
				
				const ShASTOpNodePr combined1 = ASTOpNode::create("*",opnode_left->left_,simplify_right);
				const ShASTNodePr simplified1 = combined1->simplify(); 
				if(!combined1->is_equal(simplified1)){
					return ASTOpNode::create(simplify_left->get_token(),simplified1,opnode_left->right_);
				}

				const ShASTOpNodePr combined2 = ASTOpNode::create(simplify_left->get_token(),simplify_right,opnode_left->right_);
				const ShASTNodePr simplified2 = combined2->simplify();
				if(!combined2->is_equal(simplified2)){
					return ASTOpNode::create("*",opnode_left->left_,simplified2);
				}
			}

			if(simplify_right->get_token()=="*" || simplify_right->get_token()=="/"){
				const ShASTOpNodePr opnode_right = std::dynamic_pointer_cast<ASTOpNode>(simplify_right);
				if(opnode_right==NULL)rat_throw_line("is not an operator node");
				
				const ShASTOpNodePr combined1 = ASTOpNode::create("*",opnode_right->left_,simplify_left);
				const ShASTNodePr simplified1 = combined1->simplify();
				if(!combined1->is_equal(simplified1)){
					return ASTOpNode::create(simplify_right->get_token(),simplified1,opnode_right->right_);
				}

				const ShASTOpNodePr combined2 = ASTOpNode::create(simplify_right->get_token(),simplify_left,opnode_right->right_);
				const ShASTNodePr simplified2 = combined2->simplify();
				if(!combined2->is_equal(simplified2)){
					return ASTOpNode::create("*",opnode_right->left_,simplified2);
				}
			}

			// if((simplify_left->get_token()=="*" || simplify_left->get_token()=="/") && 
			// 	(simplify_right->get_token()=="*" || simplify_right->get_token()=="/")){
				
			// 	// cast left and right operands
			// 	const ShASTOpNodePr opnode_left = std::dynamic_pointer_cast<ASTOpNode>(simplify_left);
			// 	const ShASTOpNodePr opnode_right = std::dynamic_pointer_cast<ASTOpNode>(simplify_right);
			// 	if(opnode_left==NULL)rat_throw_line("is not an operator node");
			// 	if(opnode_right==NULL)rat_throw_line("is not an operator node");
				
			// 	// combine nominators
			// 	const ShASTOpNodePr combined1 = ASTOpNode::create("*",opnode_left->left_,opnode_right->left_);
			// 	const ShASTNodePr simplified1 = combined1->simplify(); 
			// 	if(!combined1->is_equal(simplified1)){
			// 		return ASTOpNode::create(simplify_right->get_token(),ASTOpNode::create(simplify_left->get_token(),simplified1,opnode_left->right_),opnode_right->right_);
			// 	}

			// 	// combine left denominator with right numerator
			// 	const ShASTOpNodePr combined2 = ASTOpNode::create(simplify_left->get_token(),opnode_right->left_,opnode_left->right_);
			// 	const ShASTNodePr simplified2 = combined2->simplify();
			// 	if(!combined2->is_equal(simplified2)){
			// 		return ASTOpNode::create("*",ASTOpNode::create(simplify_right->get_token(),simplified2,opnode_right->right_),opnode_left->left_);
			// 	}

			// 	// combine left numerator with right denominator
			// 	const ShASTOpNodePr combined3 = ASTOpNode::create(simplify_right->get_token(),opnode_left->left_,opnode_right->right_);
			// 	const ShASTNodePr simplified3 = combined3->simplify();
			// 	if(!combined3->is_equal(simplified3)){
			// 		return ASTOpNode::create(simplify_left->get_token(),ASTOpNode::create("*",simplified3,opnode_right->left_),opnode_left->right_);
			// 	}

			// 	// combine denominators
			// 	ShASTOpNodePr combined4; std::string op;
			// 	if(simplify_left->get_token()=="/" && simplify_right->get_token()=="/"){
			// 		combined4 = ASTOpNode::create("*",opnode_left->right_, opnode_right->right_); op = "/";
			// 	}

			// 	else if(simplify_left->get_token()=="*" && simplify_right->get_token()=="/"){
			// 		combined4 = ASTOpNode::create("/", opnode_right->right_, opnode_left->right_); op = "/";
			// 	}

			// 	else if(simplify_left->get_token()=="/" && simplify_right->get_token()=="*"){
			// 		combined4 = ASTOpNode::create("/",opnode_left->right_, opnode_right->right_); op = "/";
			// 	}

			// 	else if(simplify_left->get_token()=="*" && simplify_right->get_token()=="*"){
			// 		combined4 = ASTOpNode::create("*",opnode_left->right_, opnode_right->right_); op = "*";
			// 	}

			// 	const ShASTNodePr simplified4 = combined4->simplify();
			// 	if(!combined4->is_equal(simplified4)){
			// 		return ASTOpNode::create("*",ASTOpNode::create(op, opnode_left->left_, simplified4), opnode_right->left_);
			// 	}
			// }
		}

		// division
		else if(token_=="/"){

			if(simplify_left->is_zero()){
				return ASTNode::create("0");
			}
			
			if(simplify_right->is_zero()){
				rat_throw_line("division by zero");
			}

			if(simplify_left->get_token()=="^" && simplify_right->get_token()=="^"){
				const ShASTOpNodePr opnode_left = std::dynamic_pointer_cast<ASTOpNode>(simplify_left);
				const ShASTOpNodePr opnode_right = std::dynamic_pointer_cast<ASTOpNode>(simplify_right);
				if(opnode_left==NULL)rat_throw_line("is not an operator node");
				if(opnode_right==NULL)rat_throw_line("is not an operator node");
				if(opnode_left->left_->is_equal(opnode_right->left_)){
					return ASTOpNode::create("^",opnode_left->left_,ASTOpNode::create("-",opnode_left->right_,opnode_right->right_));
				}
			}

			if(simplify_left->get_token()!="^" && simplify_right->get_token()=="^"){
				const ShASTOpNodePr opnode_right = std::dynamic_pointer_cast<ASTOpNode>(simplify_right);
				if(opnode_right==NULL)rat_throw_line("is not an operator node");
				if(opnode_right->left_->is_equal(simplify_left)){
					return ASTOpNode::create("^",opnode_right->left_,ASTOpNode::create("-",ASTNode::create("1"),opnode_right->right_));
				}
			}

			if(simplify_left->get_token()=="^" && simplify_right->get_token()!="^"){
				const ShASTOpNodePr opnode_left = std::dynamic_pointer_cast<ASTOpNode>(simplify_left);
				if(opnode_left==NULL)rat_throw_line("is not an operator node");
				if(opnode_left->left_->is_equal(simplify_right)){
					return ASTOpNode::create("^",opnode_left->left_,ASTOpNode::create("-",opnode_left->right_,ASTNode::create("1")));
				}
			}

			if(simplify_left->is_equal(simplify_right)){
				return ASTNode::create("1");
			}

			if(simplify_right->get_token()=="/"){
				const ShASTOpNodePr opnode_right = std::dynamic_pointer_cast<ASTOpNode>(simplify_right);
				if(opnode_right==NULL)rat_throw_line("is not an operator node");
				return ASTOpNode::create("*", simplify_left, ASTOpNode::create("/",opnode_right->right_,opnode_right->left_));
			}

			if(simplify_left->get_token()=="neg"){
				const ShASTFnNodePr fnnode = std::dynamic_pointer_cast<ASTFnNode>(simplify_left);
				if(fnnode==NULL)rat_throw_line("is not a function node");
				return ASTFnNode::create("neg",ASTOpNode::create("/",fnnode->get_operand(),simplify_right));
			}

			if(simplify_right->get_token()=="neg"){
				const ShASTFnNodePr fnnode = std::dynamic_pointer_cast<ASTFnNode>(simplify_right);
				if(fnnode==NULL)rat_throw_line("is not a function node");
				return ASTFnNode::create("neg",ASTOpNode::create("/",simplify_left,fnnode->get_operand()));
			}

			if(simplify_left->is_number() && simplify_right->is_number()){
				
				// get values
				const fltp a = std::stof(simplify_left->get_token());
				const fltp b = std::stof(simplify_right->get_token());
				const fltp c = a/b;


				const bool is_integer_c = std::abs(c - std::round(c))<1024*arma::Datum<fltp>::eps;

				// result can be writen as an integer
				if(is_integer_c){
					return ASTNode::create(std::to_string(int(c)));
				}

				// check if a and/or b are integer
				const bool is_integer_a = std::abs(a - std::round(a))<1024*arma::Datum<fltp>::eps;
				const bool is_integer_b = std::abs(b - std::round(b))<1024*arma::Datum<fltp>::eps;
				
				// numerator and denominator are integers
				if(is_integer_a && is_integer_b){
					// greatest common divider
					const arma::sword gcd = cmn::Extra::gcd(
						arma::sword(std::round(a)),
						arma::sword(std::round(b)));

					// if gcd equals one no simplification is possible
					if(gcd!=1){
						const ShASTOpNodePr node = ASTOpNode::create("/", 
							ASTNode::create(std::to_string(int(std::round(a/gcd)))), 
							ASTNode::create(std::to_string(int(std::round(b/gcd)))));
						return node;
					}
				}

				// a and b where not integer to start with
				else{
					std::stringstream ss; ss<<c;
					return ASTNode::create(ss.str());
				}
			}

			if(simplify_left->get_token()=="*" || simplify_left->get_token()=="/"){
				const ShASTOpNodePr opnode_left = std::dynamic_pointer_cast<ASTOpNode>(simplify_left);
				if(opnode_left==NULL)rat_throw_line("is not an operator node");
				
				const ShASTOpNodePr combined1 = ASTOpNode::create("/",opnode_left->left_,simplify_right);
				const ShASTNodePr simplified1 = combined1->simplify(); 
				if(!combined1->is_equal(simplified1)){
					return ASTOpNode::create(simplify_left->get_token(),simplified1,opnode_left->right_);
				}

				const ShASTOpNodePr combined2 = ASTOpNode::create(
					simplify_left->get_token()==token_ ? "*" : "/",opnode_left->right_,simplify_right);
				const ShASTNodePr simplified2 = combined2->simplify();
				if(!combined2->is_equal(simplified2)){
					return ASTOpNode::create(simplify_left->get_token(),opnode_left->left_,simplified2);
				}
			}

			if(simplify_right->get_token()=="*" || simplify_right->get_token()=="/"){
				const ShASTOpNodePr opnode_right = std::dynamic_pointer_cast<ASTOpNode>(simplify_right);
				if(opnode_right==NULL)rat_throw_line("is not an operator node");
				
				const ShASTOpNodePr combined1 = ASTOpNode::create("/",simplify_left,opnode_right->left_);
				const ShASTNodePr simplified1 = combined1->simplify();
				if(!combined1->is_equal(simplified1)){
					return ASTOpNode::create(simplify_right->get_token()=="*" ? "/" : "*",simplified1,opnode_right->right_);
				}

				const ShASTOpNodePr combined2 = ASTOpNode::create(simplify_right->get_token()=="*" ? "/" : "*",simplify_left,opnode_right->right_);
				const ShASTNodePr simplified2 = combined2->simplify();
				if(!combined2->is_equal(simplified2)){
					return ASTOpNode::create("/",simplified2,opnode_right->left_);
				}
			}

			// if(simplify_left->get_token()=="*" && simplify_right->get_token()=="*"){
			// 	const ShASTOpNodePr opnode_left = std::dynamic_pointer_cast<ASTOpNode>(simplify_left);
			// 	const ShASTOpNodePr opnode_right = std::dynamic_pointer_cast<ASTOpNode>(simplify_right);
			// 	if(opnode_left==NULL)rat_throw_line("is not an operator node");
			// 	if(opnode_right==NULL)rat_throw_line("is not an operator node");
			// 	const ShASTOpNodePr combined1 = ASTOpNode::create("/",opnode_left->left_,opnode_right->left_);
			// 	const ShASTNodePr simplified1 = combined1->simplify(); 
			// 	if(!combined1->is_equal(simplified1)){
			// 		return ASTOpNode::create("*",simplified1,ASTOpNode::create("/",opnode_left->right_,opnode_right->right_));
			// 	}
			// 	const ShASTOpNodePr combined2 = ASTOpNode::create("/",opnode_left->right_,opnode_right->left_);
			// 	const ShASTNodePr simplified2 = combined2->simplify();
			// 	if(!combined2->is_equal(simplified2)){
			// 		return ASTOpNode::create("*",simplified2,ASTOpNode::create("/",opnode_left->left_,opnode_right->right_));
			// 	}
			// 	const ShASTOpNodePr combined3 = ASTOpNode::create("/",opnode_left->left_,opnode_right->right_);
			// 	const ShASTNodePr simplified3 = combined3->simplify();
			// 	if(!combined3->is_equal(simplified3)){
			// 		return ASTOpNode::create("*",simplified3,ASTOpNode::create("/",opnode_left->right_,opnode_right->left_));
			// 	}
			// 	const ShASTOpNodePr combined4 = ASTOpNode::create("/",opnode_left->right_,opnode_right->right_);
			// 	const ShASTNodePr simplified4 = combined4->simplify();
			// 	if(!combined4->is_equal(simplified4)){
			// 		return ASTOpNode::create("*",simplified4,ASTOpNode::create("/",opnode_left->left_,opnode_right->left_));
			// 	}
			// }
		}

		// power
		else if(token_=="^"){
			if(simplify_left->is_zero()){
				return ASTNode::create("0");
			}

			if(simplify_right->is_one()){
				return simplify_left;
			}

			if(simplify_right->is_zero()){
				return ASTNode::create("1");
			}

			// if(simplify_right->get_token()=="^" && simplify_left->get_token()=="^"){
			// 	const ShASTOpNodePr opnode_right = std::dynamic_pointer_cast<ASTOpNode>(simplify_right);
			// 	const ShASTOpNodePr opnode_left = std::dynamic_pointer_cast<ASTOpNode>(simplify_left);
			// 	if(opnode_right==NULL)rat_throw_line("is not an operator node");
			// 	if(opnode_left==NULL)rat_throw_line("is not an operator node");
			// 	return ASTOpNode::create("^",opnode_left->left_,ASTOpNode::create("*",opnode_left->right_,ASTOpNode::create("*",opnode_right->left_,opnode_right->right_)));
			// }

			// if(simplify_right->get_token()=="^"){
			// 	const ShASTOpNodePr opnode_right = std::dynamic_pointer_cast<ASTOpNode>(simplify_right);
			// 	if(opnode_right==NULL)rat_throw_line("is not an operator node");
			// 	return ASTOpNode::create("^",simplify_left,ASTOpNode::create("*",opnode_right->left_,opnode_right->right_));
			// }

			if(simplify_left->get_token()=="^"){
				const ShASTOpNodePr opnode_left = std::dynamic_pointer_cast<ASTOpNode>(simplify_left);
				if(opnode_left==NULL)rat_throw_line("is not an operator node");
				return ASTOpNode::create("^",opnode_left->left_,ASTOpNode::create("*",opnode_left->right_,simplify_right));
			}

			if(simplify_left->is_number() && simplify_right->is_number()){
				const fltp a = std::stof(simplify_left->get_token());
				const fltp b = std::stof(simplify_right->get_token());
				const fltp c = std::pow(a,b);
				
				const bool is_integer_c = std::abs(c - std::round(c))<1024*arma::Datum<fltp>::eps;
				if(is_integer_c){
					std::stringstream ss; ss<<c;
					return ASTNode::create(ss.str());
				}

				const bool is_integer_a = std::abs(a - std::round(a))<1024*arma::Datum<fltp>::eps;
				const bool is_integer_b = std::abs(b - std::round(b))<1024*arma::Datum<fltp>::eps;
				if(!is_integer_a || !is_integer_b){
					std::stringstream ss; ss<<c;
					return ASTNode::create(ss.str());
				}
			}
		}

		// regular return
		return ASTOpNode::create(token_, simplify_left, simplify_right);
	}

	bool ASTOpNode::is_equal(const ShASTNodePr& other)const{
		if (!ASTNode::is_equal(other))return false;
		auto op_other = std::dynamic_pointer_cast<ASTOpNode>(other);
		if(op_other==NULL)return false;
		return left_->is_equal(op_other->left_) && right_->is_equal(op_other->right_);
	}

	std::string ASTOpNode::as_string()const{
		return "(" + left_->as_string() + token_ + right_->as_string() + ")";
	}



	ASTFnNode::ASTFnNode(
		const std::string& token, 
		const ShASTNodePr& operand) : ASTNode(token){
		operand_ = operand;
	}

	ShASTFnNodePr ASTFnNode::create(
		const std::string& token, 
		const ShASTNodePr& operand){
		return std::make_shared<ASTFnNode>(token, operand);
	}

	const ShASTNodePr& ASTFnNode::get_operand()const{
		return operand_;
	}

	ShASTNodePr ASTFnNode::calculate_derivative(
		const std::string& towards_token,
		const arma::uword depth)const{
		const ShASTNodePr operand_derivative = operand_->calculate_derivative(towards_token,depth+1);
		
		// // unit function	
		// if(token_=="unit")
		// 	return ASTOpNode::create("*",ASTNode::create("0"),operand_);

		// trigonemetric function
		if(token_=="sin")
			return ASTOpNode::create("*",operand_derivative,ASTFnNode::create("cos",operand_));
		else if(token_=="cos")
			return ASTFnNode::create("neg",ASTOpNode::create("*",operand_derivative,ASTFnNode::create("sin",operand_)));
		else if(token_=="tan")
			return ASTOpNode::create("*",operand_derivative,ASTOpNode::create("^",ASTFnNode::create("sec",operand_),ASTNode::create("2")));
		
		// secans and cosecans
		else if(token_=="sec")
			return ASTOpNode::create("*",operand_derivative,ASTOpNode::create("*",ASTFnNode::create("sec",operand_),ASTFnNode::create("tan",operand_)));
		else if(token_=="csc")
			return ASTFnNode::create("neg",ASTOpNode::create("*",operand_derivative,ASTOpNode::create("*",ASTFnNode::create("cot",operand_),ASTFnNode::create("csc",operand_))));

		// cotangens
		else if(token_=="cot")
			return ASTFnNode::create("neg",ASTOpNode::create("*",operand_derivative,ASTOpNode::create("^",ASTFnNode::create("csc",operand_),ASTNode::create("2"))));

		// inverse trig
		if(token_=="asin")
			return ASTOpNode::create("/",
				operand_derivative,
				ASTFnNode::create("sqrt",ASTOpNode::create("-", ASTNode::create("1"),
				ASTOpNode::create("^",operand_,ASTNode::create("2")))));
		
		if(token_=="acos")
			return ASTOpNode::create("/",
				ASTFnNode::create("neg",operand_derivative),
				ASTFnNode::create("sqrt",ASTOpNode::create("-", ASTNode::create("1"),
				ASTOpNode::create("^",operand_,ASTNode::create("2")))));
		
		if(token_=="atan")
			return ASTOpNode::create("/",
				operand_derivative,
				ASTOpNode::create("+",ASTNode::create("1"),
				ASTOpNode::create("^",operand_,ASTNode::create("2"))));
		

		// exponential functions
		if(token_=="exp")
			return ASTOpNode::create("*",ASTFnNode::create("exp",operand_), operand_derivative);
		if(token_=="log")
			return ASTOpNode::create("/",operand_derivative,operand_);

		// roots
		if (token_ == "sqrt")
			return ASTOpNode::create("/",operand_derivative, ASTOpNode::create("*",ASTNode::create("2"),ASTFnNode::create("sqrt",operand_)));
		

		if (token_ == "cbrt")
			return ASTOpNode::create("/", operand_derivative, ASTOpNode::create("*",ASTNode::create("3"), ASTOpNode::create("^", operand_, ASTOpNode::create("/",ASTNode::create("2"),ASTNode::create("3")))));
		
		// sign related functions
		if (token_ == "abs")
			return ASTOpNode::create("*", operand_derivative, ASTFnNode::create("sign", operand_));
		if (token_ == "sign")
			return ASTOpNode::create("*",ASTNode::create("0"),operand_);
		if(token_=="neg"){
			if(operand_derivative->is_zero()){
				return ASTNode::create("0");
			}else{
				return ASTFnNode::create("neg",operand_derivative);
			}
		}

		// hyperbolic functions
		if (token_ == "sinh")
			return ASTOpNode::create("*",operand_derivative, ASTFnNode::create("cosh", operand_));
		if (token_ == "cosh")
			return ASTOpNode::create("*",operand_derivative, ASTFnNode::create("sinh", operand_));
		if (token_ == "tanh")
			return ASTOpNode::create("/",operand_derivative, ASTOpNode::create("^", ASTFnNode::create("cosh", operand_), ASTNode::create("2")));

		// step function
		if (token_=="step")
			return ASTNode::create("0");

		// not recognized
		rat_throw_line("function not recognized: " + token_);
	}

	std::list<std::string> ASTFnNode::to_rpn_notation()const{
		std::list<std::string> rpn;
		rpn.splice(rpn.end(), operand_->to_rpn_notation());
		rpn.push_back(token_);
		return rpn;
	}

	ShASTNodePr ASTFnNode::simplify(const arma::uword depth)const{
		// simplify children
		const ShASTNodePr simplified_operand = operand_->simplify(depth+1);

		// child is zero
		if(simplified_operand->is_zero()){
			
			// some functions return zero upon zero input
			if(token_=="sin" || token_=="abs" || token_=="sqrt" || 
				token_=="cbrt" || token_=="neg" || token_=="sign" || 
				token_=="sinh" || token_=="tan" || token_=="tanh"){
				return ASTNode::create("0");
			}

			else if(token_=="exp" || token_=="cos" || token_=="cosh"){
				return ASTNode::create("1");
			}

			// function of zero
			ASTFnNode::create(token_, ASTNode::create("0"));
		}

		if(simplified_operand->is_one()){
			if(token_=="abs" || token_=="sign"){
				return ASTNode::create("1");
			}
		}

		// double abs is not needed
		if(token_=="abs" && simplified_operand->get_token()=="abs"){
			return simplified_operand;
		}

		// double neg is positive
		if(token_=="neg" && simplified_operand->get_token()=="neg"){
			const ShASTFnNodePr fnnode = std::dynamic_pointer_cast<ASTFnNode>(simplified_operand);
			if(fnnode==NULL)rat_throw_line("is not a function node");
			return fnnode->get_operand();
		}

		// child is not zero
		return ASTFnNode::create(token_, simplified_operand);
	}


	bool ASTFnNode::is_equal(const ShASTNodePr& other) const {
		if(!ASTNode::is_equal(other))return false;
		auto fn_node = std::dynamic_pointer_cast<ASTFnNode>(other);
		if(fn_node==NULL)return false;
		return operand_->is_equal(fn_node->operand_);
	}

	std::string ASTFnNode::as_string()const{
		return token_ + "(" + operand_->as_string() + ")";
	}

}}
