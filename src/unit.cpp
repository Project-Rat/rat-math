// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "unit.hh"

// general headers
#include <list>
#include <cassert>

// rat common headers
#include "rat/common/error.hh"

// code specific to Rat
namespace rat{namespace math{

	// Static Unit constants	
	const Unit Unit::D =   Unit({0, 0, 0, 0, 0, 0, 0});
	const Unit Unit::s =   Unit({1, 0, 0, 0, 0, 0, 0});
	const Unit Unit::m =   Unit({0, 1, 0, 0, 0, 0, 0});
	const Unit Unit::kg =  Unit({0, 0, 1, 0, 0, 0, 0});
	const Unit Unit::A =   Unit({0, 0, 0, 1, 0, 0, 0});
	const Unit Unit::K =   Unit({0, 0, 0, 0, 1, 0, 0});
	const Unit Unit::mol = Unit({0, 0, 0, 0, 0, 1, 0});
	const Unit Unit::cd =  Unit({0, 0, 0, 0, 0, 0, 1});

	// constructor
	Unit::Unit() {

	}

	// constructor
	Unit::Unit(const arma::Row<fltp>& dim){
		dim_ = dim;
	}

	bool Unit::operator==(const Unit& r) const {
		if(dim_.size()!=r.dim_.size())rat_throw_line("Dimension mismatch");
		return arma::all(arma::abs(dim_ - r.dim_)<1e-9);
	}

	bool Unit::operator!=(const Unit& r) const {
		return !(*this==r);
	}

	Unit Unit::operator+(const Unit& r) const {
		if (*this == r) return *this;
		rat_throw_line("Unit mismatch in operator +");
	}

	Unit Unit::operator-(const Unit& r) const {
		if (*this == r) return *this;
		rat_throw_line("Unit mismatch in operator -");
	}

	Unit Unit::operator/(const Unit& r) const {
		if (dim_.n_elem != r.dim_.n_elem)
			rat_throw_line("Dimension mismatch");
		Unit result = *this;
		for (arma::uword i = 0; i < dim_.n_elem; i++)
			result.dim_(i) -= r.dim_(i);
		return result;
	}

	Unit Unit::operator*(const Unit& r) const {
		if (dim_.n_elem != r.dim_.n_elem)
			rat_throw_line("Dimension mismatch");
		Unit result = *this;
		for (arma::uword i = 0; i < dim_.n_elem; i++)
			result.dim_(i) += r.dim_(i);
		return result;
	}

	Unit Unit::operator%(const Unit& r) const {
		if (*this==r)return *this;
		rat_throw_line("Unit mismatch in operator %");
	}

	Unit Unit::operator^(const fltp f) const {
		Unit result = *this;
		result.dim_*=f;
		return result;
	}

	Unit Unit::operator^(const arma::uword f) const {
		Unit result = *this;
		result.dim_*=static_cast<fltp>(f);
		return result;
	}

	Unit Unit::operator^(const int f) const {
		Unit result = *this;
		result.dim_*=f;
		return result;
	}

	// get dimensions
	const arma::Row<fltp>& Unit::get_dim()const{
		return dim_;
	}

	// get extended dimensions
	arma::Row<fltp> Unit::get_ext_dim()const{
		// dimensions
		arma::Row<fltp> ext_dim = arma::reshape(get_dim(),1,17); 

		// early out
		if(arma::all(ext_dim==0))return ext_dim;

		// s, m, kg, A, K, mol, cd,  J, N, W, V, Ohm, H, F, Pa, C, T
		const arma::field<arma::Row<fltp> > extended_units = {	
			{-2, 2, 1, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0},    // Joules: kg m^2 s^-2
			{-2, 1, 1, 0, 0, 0, 0,  0,-1, 0, 0, 0, 0, 0, 0, 0, 0},    // Newtons: kg m s^-2
			{ 0,-1, 0, 0, 0, 0, 0,  1,-1, 0, 0, 0, 0, 0, 0, 0, 0},    // Newtons: J/m
			// { 0, 1, 0, 1, 0, 0, 0,  0,-1, 0, 0, 0, 0, 0, 0, 0, 1},    // Newton: T*A*m
			{-3, 2, 1, 0, 0, 0, 0,  0, 0,-1, 0, 0, 0, 0, 0, 0, 0},    // Watts: kg m^2 s^-3
			{-1, 0, 0, 0, 0, 0, 0,  1, 0,-1, 0, 0, 0, 0, 0, 0, 0},    // Watts: J/s
			{ 0, 0,-1, 0, 0, 0, 0,  0, 2,-1, 0, 0, 0, 0, 0, 0, 0},    // Watts: N^2 kg-1
			{-3, 2, 1,-1, 0, 0, 0,  0, 0, 0,-1, 0, 0, 0, 0, 0, 0},    // Volts: kg m^2 s^-3 A^-1
			{ 0, 0, 0,-1, 0, 0, 0,  0, 0, 1,-1, 0, 0, 0, 0, 0, 0},    // Volts: W/A
			{-3, 2, 1,-2, 0, 0, 0,  0, 0, 0, 0,-1, 0, 0, 0, 0, 0},    // Ohms: kg m^2 s^-3 A^-2
			{ 0, 0, 0,-1, 0, 0, 0,  0, 0, 0, 1,-1, 0, 0, 0, 0, 0},    // Ohms: V/A
			{-2, 2, 1,-2, 0, 0, 0,  0, 0, 0, 0, 0,-1, 0, 0, 0, 0},    // Henrys: kg m^2 s^-2 A^-2
			{ 0, 0, 0,-2, 0, 0, 0,  1, 0, 0, 0, 0,-1, 0, 0, 0, 0},    // Henrys: J/A^2
			{ 4,-2,-1, 2, 0, 0, 0,  0, 0, 0, 0, 0, 0,-1, 0, 0, 0},    // Farads: kg^-1 m^-2 s^4 A^2
			{ 1, 0, 0, 0, 0, 0, 0,  0, 0, 0, 0, 1, 0,-1, 0, 0, 0},    // Farads: Ohm s
			// {-2,-1, 1, 0, 0, 0, 0,  0, 0, 0, 0, 0, 0, 0,-1, 0, 0},    // Pascals: kg m^-1 s^-2
			// { 0,-2, 0, 0, 0, 0, 0,  0, 1, 0, 0, 0, 0, 0,-1, 0, 0},    // Pascals: N/m^2
			{ 1, 0, 0, 1, 0, 0, 0,  0, 0, 0, 0, 0, 0, 0, 0,-1, 0},	 // Coulombs: s A
			{-2, 0, 1,-1, 0, 0, 0,  0, 0, 0, 0, 0, 0, 0, 0, 0,-1},    // Tesla: kg s^-2 A^-1
			{ 0,-1, 0,-1, 0, 0, 0,  0, 1, 0, 0, 0, 0, 0, 0, 0,-1},    // Tesla: N m^-1 A^-1
		};

		const arma::Row<fltp> weight{1,1,1,1,1,1,1, 1.5,1.0,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5}; 

		// check
		assert(weight.n_elem==extended_units(0).n_elem);

		// check for change
		for(arma::uword i=0;i<100;i++){
			// score without change
			const fltp score = arma::accu(arma::abs(ext_dim)%weight);

			// no improvement possible 
			if(score<=1)break;

			// keep list
			arma::Row<fltp> add_score(extended_units.n_elem);
			arma::Row<fltp> sub_score(extended_units.n_elem);

			// walk over list and try changes
			for(arma::uword i=0;i<extended_units.n_elem;i++){
				arma::Row<fltp> add = ext_dim + extended_units(i);
				arma::Row<fltp> sub = ext_dim - extended_units(i);
				add_score(i) = arma::accu(arma::abs(add)%weight);
				sub_score(i) = arma::accu(arma::abs(sub)%weight);
			}

			// calculate max change
			const fltp max_add_score = arma::min(add_score);
			const fltp max_sub_score = arma::min(sub_score);

			// no improvement possible
			if(std::min(max_sub_score, max_add_score)>=score)break;

			// add or subtract best result
			if(max_add_score<max_sub_score)
				ext_dim += extended_units(arma::index_min(add_score));
			else
				ext_dim -= extended_units(arma::index_min(sub_score));
		}

		return ext_dim;
	}


	void Unit::uprint(std::ostream& o, const std::string& x, const fltp f){
		if(std::abs(f)<1e-12) return;
		if(std::abs(f-1)<1e-12) o << " " << x;
		else o << " " << x << "^" << f;
	}
		
	// convert to string
	std::string Unit::as_string()const{
		const arma::Row<fltp> dim = get_ext_dim();
			
		// create stream
		std::stringstream o;

		// extended units
		Unit::uprint(o, "J", dim(7));
		Unit::uprint(o, "N", dim(8));
		Unit::uprint(o, "W", dim(9));
		Unit::uprint(o, "V", dim(10));
		Unit::uprint(o, "Ohm", dim(11));
		Unit::uprint(o, "H", dim(12));
		Unit::uprint(o, "F", dim(13));
		Unit::uprint(o, "Pa", dim(14));
		Unit::uprint(o, "C", dim(15));
		Unit::uprint(o, "T", dim(16));

		// standard units
		Unit::uprint(o, "kg", dim(2));
		Unit::uprint(o, "m", dim(1));
		Unit::uprint(o, "A", dim(3));
		Unit::uprint(o, "K", dim(4));
		Unit::uprint(o, "mol", dim(5));
		Unit::uprint(o, "cd", dim(6));
		Unit::uprint(o, "s", dim(0));

		const std::string str = o.str();

		if(str.size()>0)return str.substr(1);
		return str;
	}

	std::ostream& operator<<(std::ostream& o, const Unit& u){
		return o<<u.as_string();
	}

}}