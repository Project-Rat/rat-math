// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// header
#include "equation.hh"

// rat common headers
#include "rat/common/error.hh"

// rat math headers
#include "parser.hh"

// rat specific
namespace rat{namespace math{

	// default constructor
	Equation::Equation(){
		
	}

	// assigning from string or char
	Equation::Equation(const std::string& expression){
		expression_ = expression;
		unit_assigned_ = true;
	}

	Equation::Equation(const char* expression){
		expression_ = std::string(expression);
		unit_assigned_ = true;
	}

	Equation::Equation(const fltp value){
		expression_ = std::to_string(value);
		unit_assigned_ = false;
	}

	Equation::Equation(const Equation& eq){
		expression_ = eq.expression_;
		unit_assigned_ = eq.unit_assigned_;
	}

	// assigning from json file
	void Equation::operator=(const Json::Value& js){
		// assign string
		if(js.isString()){
			expression_ = js.asString();
			unit_assigned_ = true;
		}

		// assign from value
		else if(js.isNumeric()){
			const Value oldval = Parser::create()->evaluate_to_scalar(expression_);
			expression_ = std::to_string(js.ASFLTP());
			const std::string unit = oldval.get_unit().as_string();
			if(!unit.empty()){
				expression_ += " " + unit;
				unit_assigned_ = true;
			}else{
				unit_assigned_ = false;
			}
		}

		// json contains something else
		else{
			rat_throw_line("json is not a string or value");
		}
	}

	// assigning from json file
	void Equation::operator=(const Equation& eq){
		if(!eq.unit_assigned_ && unit_assigned_){
			const Value oldval = Parser::create()->evaluate_to_scalar(expression_);
			expression_ = eq.expression_;
			const std::string unit = oldval.get_unit().as_string();
			expression_ += " " + unit;
		}else{
			expression_ = eq.expression_;
			unit_assigned_ = eq.unit_assigned_;
		}
	}

	// get expression
	const std::string& Equation::get_expression()const{
		return expression_;
	}

	// // get derivative
	// Equation Equation::calc_derivative(const std::string& towards, const ShParserPr& parser)const{
	// 	return Equation(parser->calc_derivative(expression_,towards));
	// }

	// // evaluate to scalar output to Value object
	// Value Equation::evaluate_scalar(const ShParserPr& parser)const{
	// 	// array
	// 	const arma::field<Value> array = parser->evaluate(expression_);

	// 	// check scalar output
	// 	if(array.n_elem>1)rat_throw_line("non-scalar output");

	// 	// return first value
	// 	return array.front();
	// }

	// // evaluate and expect scalar output with given unit
	// fltp Equation::evaluate_scalar(const Unit& expected_unit, const ShParserPr& parser)const{
	// 	Value val = evaluate_scalar(parser);
	// 	if(val.get_unit()!=expected_unit)rat_throw_line(
	// 		"evaluated unit " + val.get_unit().as_string() + 
	// 		" does not equate to expected unit " + expected_unit.as_string());
	// 	return val.get_number();
	// }

	// // evaluate using callable operator
	// Value Equation::operator()(const ShParserPr& parser)const{
	// 	return evaluate_scalar(parser);
	// }

	// // evaluate using callable operator
	// Value Equation::operator()(const Unit& expected_unit, const ShParserPr& parser)const{
	// 	return evaluate_scalar(expected_unit,parser);
	// }

	std::ostream& operator<<(std::ostream& o, const Equation& eq){
		return (o<<eq.get_expression());
	}

}}
