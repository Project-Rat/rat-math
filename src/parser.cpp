// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// header
#include "parser.hh"

// general headers
#include <regex>
#include <stack>
#include <iostream>
#include <algorithm>
#include <cctype>
#include <sstream>
#include <functional>

// rat common headers
#include "rat/common/error.hh"
#include "rat/common/extra.hh"

// rat math headers
#include "equation.hh"


namespace rat{namespace math{
	// constructor
	Parser::Parser(){
		load_default_functions();
		load_default_unit_table();
		load_default_operators();
	}

	// factory
	ShParserPr Parser::create(){
		return std::make_shared<Parser>();
	}

	// load default functions
	void Parser::load_default_functions(){
		// function map
		fnmap_ = std::map<std::string, std::function<arma::field<Value>(const arma::field<arma::field<Value> >&)> >{
			// 1 ARGUMENT FUNCTION
			// extract unit
			{"unit", [](const arma::field<arma::field<Value> >& args){ 
				if (args.n_elem!=1) rat_throw_line("unit requires exactly one argument");
				const arma::field<Value>& arg = args(0);
				arma::field<Value> output(arg.n_rows, arg.n_cols);
				for(arma::uword i=0;i<arg.n_elem;i++){
					output(i) = Value(1.0, arg(i).get_unit());
				}
				return output;
			}},

			// trigonometric functions
			{"sin", [](const arma::field<arma::field<Value> >& args){ 
				if (args.n_elem != 1) rat_throw_line("sin requires exactly one argument");
				const arma::field<Value>& arg = args(0);
				arma::field<Value> output(arg.n_rows, arg.n_cols);
				for(arma::uword i=0;i<arg.n_elem;i++){
					if(arg(i).get_unit()!=Unit::D)rat_throw_line("input of sin must be unitless");
					output(i) = Value(
						std::sin(arg(i).get_number()),
						Unit::D);
				}
				return output;
			}},
			
			{"cos", [](const arma::field<arma::field<Value> >& args){ 
				if (args.n_elem != 1) rat_throw_line("cos requires exactly one argument");
				const arma::field<Value>& arg = args(0);
				arma::field<Value> output(arg.n_rows, arg.n_cols);
				for(arma::uword i=0;i<arg.n_elem;i++){
					if(arg(i).get_unit()!=Unit::D)rat_throw_line("input of cos must be unitless");
					output(i) = Value(
						std::cos(arg(i).get_number()),
						Unit::D);
				}
				return output;
			}},

			{"tan", [](const arma::field<arma::field<Value> >& args){ 
				if (args.n_elem != 1) rat_throw_line("tan requires exactly one argument");
				const arma::field<Value>& arg = args(0);
				arma::field<Value> output(arg.n_rows, arg.n_cols);
				for(arma::uword i=0;i<arg.n_elem;i++){
					if(arg(i).get_unit()!=Unit::D)rat_throw_line("input of tan must be unitless");
					output(i) = Value(
						std::cos(arg(i).get_number()),
						Unit::D);
				}
				return output;
			}},

			{"sec", [](const arma::field<arma::field<Value> >& args){ 
				if (args.n_elem != 1) rat_throw_line("sec requires exactly one argument");
				const arma::field<Value>& arg = args(0);
				arma::field<Value> output(arg.n_rows, arg.n_cols);
				for(arma::uword i=0;i<arg.n_elem;i++){
					if(arg(i).get_unit()!=Unit::D)rat_throw_line("input of sec must be unitless");
					output(i) = Value(
						RAT_CONST(1.0)/std::cos(arg(i).get_number()),
						Unit::D);
				}
				return output;
			}},

			{"csc", [](const arma::field<arma::field<Value> >& args){ 
				if (args.n_elem != 1) rat_throw_line("csc requires exactly one argument");
				const arma::field<Value>& arg = args(0);
				arma::field<Value> output(arg.n_rows, arg.n_cols);
				for(arma::uword i=0;i<arg.n_elem;i++){
					if(arg(i).get_unit()!=Unit::D)rat_throw_line("input of csc must be unitless");
					output(i) = Value(
						RAT_CONST(1.0)/std::sin(arg(i).get_number()),
						Unit::D);
				}
				return output;
			}},

			{"cot", [](const arma::field<arma::field<Value> >& args){ 
				if (args.n_elem != 1) rat_throw_line("cot requires exactly one argument");
				const arma::field<Value>& arg = args(0);
				arma::field<Value> output(arg.n_rows, arg.n_cols);
				for(arma::uword i=0;i<arg.n_elem;i++){
					if(arg(i).get_unit()!=Unit::D)rat_throw_line("input of cot must be unitless");
					output(i) = Value(
						RAT_CONST(1.0)/std::tan(arg(i).get_number()),
						Unit::D);
				}
				return output;
			}},	


			// inverse trigonometric functions
			{"asin", [](const arma::field<arma::field<Value> >& args){ 
				if (args.n_elem != 1) rat_throw_line("asin requires exactly one argument");
				const arma::field<Value>& arg = args(0);
				arma::field<Value> output(arg.n_rows, arg.n_cols);
				for(arma::uword i=0;i<arg.n_elem;i++){
					if(arg(i).get_unit()!=Unit::D)rat_throw_line("input of asin must be unitless");
					output(i) = Value(
						std::asin(arg(i).get_number()),
						Unit::D);
				}
				return output;
			}},	


			{"acos", [](const arma::field<arma::field<Value> >& args){ 
				if (args.n_elem != 1) rat_throw_line("acos requires exactly one argument");
				const arma::field<Value>& arg = args(0);
				arma::field<Value> output(arg.n_rows, arg.n_cols);
				for(arma::uword i=0;i<arg.n_elem;i++){
					if(arg(i).get_unit()!=Unit::D)rat_throw_line("input of acos must be unitless");
					output(i) = Value(
						std::acos(arg(i).get_number()),
						Unit::D);
				}
				return output;
			}},	

			{"atan", [](const arma::field<arma::field<Value> >& args){ 
				if (args.n_elem != 1) rat_throw_line("atan requires exactly one argument");
				const arma::field<Value>& arg = args(0);
				arma::field<Value> output(arg.n_rows, arg.n_cols);
				for(arma::uword i=0;i<arg.n_elem;i++){
					if(arg(i).get_unit()!=Unit::D)rat_throw_line("input of atan must be unitless");
					output(i) = Value(
						std::atan(arg(i).get_number()),
						Unit::D);
				}
				return output;
			}},	


			// hyperbolic functions
			{"sinh", [](const arma::field<arma::field<Value> >& args){ 
				if (args.n_elem != 1) rat_throw_line("sinh requires exactly one argument");
				const arma::field<Value>& arg = args(0);
				arma::field<Value> output(arg.n_rows, arg.n_cols);
				for(arma::uword i=0;i<arg.n_elem;i++){
					if(arg(i).get_unit()!=Unit::D)rat_throw_line("input of sinh must be unitless");
					output(i) = Value(
						std::sinh(arg(i).get_number()),
						Unit::D);
				}
				return output;
			}},	

			{"cosh", [](const arma::field<arma::field<Value> >& args){ 
				if (args.n_elem != 1) rat_throw_line("cosh requires exactly one argument");
				const arma::field<Value>& arg = args(0);
				arma::field<Value> output(arg.n_rows, arg.n_cols);
				for(arma::uword i=0;i<arg.n_elem;i++){
					if(arg(i).get_unit()!=Unit::D)rat_throw_line("input of cosh must be unitless");
					output(i) = Value(
						std::cosh(arg(i).get_number()),
						Unit::D);
				}
				return output;
			}},	

			{"tanh", [](const arma::field<arma::field<Value> >& args){ 
				if (args.n_elem != 1) rat_throw_line("tanh requires exactly one argument");
				const arma::field<Value>& arg = args(0);
				arma::field<Value> output(arg.n_rows, arg.n_cols);
				for(arma::uword i=0;i<arg.n_elem;i++){
					if(arg(i).get_unit()!=Unit::D)rat_throw_line("input of tanh must be unitless");
					output(i) = Value(
						std::tanh(arg(i).get_number()),
						Unit::D);
				}
				return output;
			}},	


			// inverse hyperbolic functions
			{"asinh", [](const arma::field<arma::field<Value> >& args){ 
				if (args.n_elem != 1) rat_throw_line("asinh requires exactly one argument");
				const arma::field<Value>& arg = args(0);
				arma::field<Value> output(arg.n_rows, arg.n_cols);
				for(arma::uword i=0;i<arg.n_elem;i++){
					if(arg(i).get_unit()!=Unit::D)rat_throw_line("input of asinh must be unitless");
					output(i) = Value(
						std::asinh(arg(i).get_number()),
						Unit::D);
				}
				return output;
			}},	

			{"acosh", [](const arma::field<arma::field<Value> >& args){ 
				if (args.n_elem != 1) rat_throw_line("acosh requires exactly one argument");
				const arma::field<Value>& arg = args(0);
				arma::field<Value> output(arg.n_rows, arg.n_cols);
				for(arma::uword i=0;i<arg.n_elem;i++){
					if(arg(i).get_unit()!=Unit::D)rat_throw_line("input of acosh must be unitless");
					output(i) = Value(
						std::acosh(arg(i).get_number()),
						Unit::D);
				}
				return output;
			}},	

			{"atanh", [](const arma::field<arma::field<Value> >& args){ 
				if (args.n_elem != 1) rat_throw_line("atanh requires exactly one argument");
				const arma::field<Value>& arg = args(0);
				arma::field<Value> output(arg.n_rows, arg.n_cols);
				for(arma::uword i=0;i<arg.n_elem;i++){
					if(arg(i).get_unit()!=Unit::D)rat_throw_line("input of atanh must be unitless");
					output(i) = Value(
						std::atanh(arg(i).get_number()),
						Unit::D);
				}
				return output;
			}},	
			
			// root functions
			{"sqrt", [](const arma::field<arma::field<Value> >& args){ 
				if (args.n_elem != 1) rat_throw_line("sqrt requires exactly one argument");
				const arma::field<Value>& arg = args(0);
				arma::field<Value> output(arg.n_rows, arg.n_cols);
				for(arma::uword i=0;i<arg.n_elem;i++){
					output(i) = Value(
						std::sqrt(arg(i).get_number()),
						arg(i).get_unit()^(1.0/2));
				}
				return output;
			}},	

			{"cbrt", [](const arma::field<arma::field<Value> >& args){ 
				if (args.n_elem != 1) rat_throw_line("cbrt requires exactly one argument");
				const arma::field<Value>& arg = args(0);
				arma::field<Value> output(arg.n_rows, arg.n_cols);
				for(arma::uword i=0;i<arg.n_elem;i++){
					if(arg(i).get_unit()!=Unit::D)rat_throw_line("input of cbrt must be unitless");
					output(i) = Value(
						std::sqrt(arg(i).get_number()),
						arg(i).get_unit()^(1.0/3));
				}
				return output;
			}},	


			// exponents
			{"exp", [](const arma::field<arma::field<Value> >& args){ 
				if (args.n_elem != 1) rat_throw_line("exp requires exactly one argument");
				const arma::field<Value>& arg = args(0);
				arma::field<Value> output(arg.n_rows, arg.n_cols);
				for(arma::uword i=0;i<arg.n_elem;i++){
					if(arg(i).get_unit()!=Unit::D)rat_throw_line("input of exp must be unitless");
					output(i) = Value(
						std::exp(arg(i).get_number()),
						Unit::D);
				}
				return output;
			}},	

		

			// logarithmic functions
			{"log", [](const arma::field<arma::field<Value> >& args){ 
				if (args.n_elem != 1) rat_throw_line("log requires exactly one argument");
				const arma::field<Value>& arg = args(0);
				arma::field<Value> output(arg.n_rows, arg.n_cols);
				for(arma::uword i=0;i<arg.n_elem;i++){
					if(arg(i).get_unit()!=Unit::D)rat_throw_line("input of log must be unitless");
					output(i) = Value(
						std::log(arg(i).get_number()),
						Unit::D);
				}
				return output;
			}},	

			{"log2", [](const arma::field<arma::field<Value> >& args){ 
				if (args.n_elem != 1) rat_throw_line("log2 requires exactly one argument");
				const arma::field<Value>& arg = args(0);
				arma::field<Value> output(arg.n_rows, arg.n_cols);
				for(arma::uword i=0;i<arg.n_elem;i++){
					if(arg(i).get_unit()!=Unit::D)rat_throw_line("input of log2 must be unitless");
					output(i) = Value(
						std::log2(arg(i).get_number()),
						Unit::D);
				}
				return output;
			}},	

			{"log10", [](const arma::field<arma::field<Value> >& args){ 
				if (args.n_elem != 1) rat_throw_line("log10 requires exactly one argument");
				const arma::field<Value>& arg = args(0);
				arma::field<Value> output(arg.n_rows, arg.n_cols);
				for(arma::uword i=0;i<arg.n_elem;i++){
					if(arg(i).get_unit()!=Unit::D)rat_throw_line("input of log10 must be unitless");
					output(i) = Value(
						std::log10(arg(i).get_number()),
						Unit::D);
				}
				return output;
			}},	


			// rounding functions
			{"ceil", [](const arma::field<arma::field<Value> >& args){ 
				if (args.n_elem != 1) rat_throw_line("ceil requires exactly one argument");
				const arma::field<Value>& arg = args(0);
				arma::field<Value> output(arg.n_rows, arg.n_cols);
				for(arma::uword i=0;i<arg.n_elem;i++)
					output(i) = Value(std::ceil(arg(i).get_number()), arg(i).get_unit());
				return output;
			}},	

			{"floor", [](const arma::field<arma::field<Value> >& args){ 
				if (args.n_elem != 1) rat_throw_line("floor requires exactly one argument");
				const arma::field<Value>& arg = args(0);
				arma::field<Value> output(arg.n_rows, arg.n_cols);
				for(arma::uword i=0;i<arg.n_elem;i++)
					output(i) = Value(std::floor(arg(i).get_number()), arg(i).get_unit());
				return output;
			}},	

			{"round", [](const arma::field<arma::field<Value> >& args){ 
				if (args.n_elem != 1) rat_throw_line("round requires exactly one argument");
				const arma::field<Value>& arg = args(0);
				arma::field<Value> output(arg.n_rows, arg.n_cols);
				for(arma::uword i=0;i<arg.n_elem;i++)
					output(i) = Value(std::round(arg(i).get_number()), arg(i).get_unit());
				return output;
			}},	


			// sign functions
			{"sign", [](const arma::field<arma::field<Value> >& args){ 
				if (args.n_elem != 1) rat_throw_line("sign requires exactly one argument");
				const arma::field<Value>& arg = args(0);
				arma::field<Value> output(arg.n_rows, arg.n_cols);
				for(arma::uword i=0;i<arg.n_elem;i++)
					output(i) = Value(cmn::Extra::sign(arg(i).get_number()), arg(i).get_unit());
				return output;
			}},	

			{"abs", [](const arma::field<arma::field<Value> >& args){ 
				if (args.n_elem != 1) rat_throw_line("abs requires exactly one argument");
				const arma::field<Value>& arg = args(0);
				arma::field<Value> output(arg.n_rows, arg.n_cols);
				for(arma::uword i=0;i<arg.n_elem;i++)
					output(i) = Value(std::abs(arg(i).get_number()), arg(i).get_unit());
				return output;
			}},	

			{"neg", [](const arma::field<arma::field<Value> >& args){ 
				if (args.n_elem != 1) rat_throw_line("abs requires exactly one argument");
				const arma::field<Value>& arg = args(0);
				arma::field<Value> output(arg.n_rows, arg.n_cols);
				for(arma::uword i=0;i<arg.n_elem;i++)
					output(i) = Value(-arg(i).get_number(), arg(i).get_unit());
				return output;
			}},	

			
			// transposition
			{"trans", [](const arma::field<arma::field<Value> >& args){ 
				if (args.n_elem != 1)rat_throw_line("sin requires exactly one argument");
				arma::field<Value> arg = args(0);
				arma::field<Value> output(arg.n_cols, arg.n_rows);
				for(arma::uword j=0;j<arg.n_cols;j++){
					for(arma::uword i=0;i<arg.n_rows;i++){
						output(j,i) = arg(i,j);
					}
				}
				return output;
			}},




			// signals
			{"step", [](const arma::field<arma::field<Value> >& args){ 
				if (args.n_elem != 1) rat_throw_line("abs requires exactly one argument");
				const arma::field<Value>& arg = args(0);
				arma::field<Value> output(arg.n_rows, arg.n_cols);
				for(arma::uword i=0;i<arg.n_elem;i++)
					output(i) = Value(arg(i).get_number()<RAT_CONST(0.0) ? RAT_CONST(0.0) : RAT_CONST(1.0), arg(i).get_unit());
				return output;
			}},	
		
	

			// 2 ARGUMENT FUNCTION
			// calculate power pow(val,exp)
			{"pow", [](const arma::field<arma::field<Value> >& args){
				// get individual arguments
				if(args.n_elem!=2)rat_throw_line("pow requires exactly two arguments");
				const arma::field<Value>& arg1 = args(0);
				const arma::field<Value>& arg2 = args(1);

				// output
				arma::field<Value> output(arg1.n_rows, arg1.n_cols);

				// raise all elements in A to the power denoted by the given scalar
				if(arg2.n_elem==1){
					if(arg2(0).get_unit()!=Unit::D)
						rat_throw_line("in pow function exponent must be unitless");
					for(arma::uword i=0;i<arg1.n_elem;i++){
						output(i) = Value(
							std::pow(arg1(i).get_number(), arg2(0).get_number()), 
							arg1(i).get_unit()^arg2(0).get_number());
					}
				}

				// raise each element in A to the power denoted by the corresponding element in B; 
				// the sizes of A and B must be the same
				else if(arg1.n_rows==arg2.n_rows && arg2.n_cols==arg1.n_cols){
					for(arma::uword i=0;i<arg1.n_elem;i++){
						if(arg2(i).get_unit()!=Unit::D)
							rat_throw_line("in pow function exponent must be unitless");
						output(i) = Value(
							std::pow(arg1(i).get_number(), arg2(i).get_number()), 
							arg1(i).get_unit()^arg2(i).get_number());
					}
				}

				// no match
				else{
					rat_throw_line("pow matrix sizes are not in agreement");
				}

				// return output
				return output;
			}},

			// calculate hypot function
			{"hypot", [](const  arma::field<arma::field<Value> >& args){
				// get individual arguments
				if(args.n_elem!=2)rat_throw_line("hypot requires exactly two arguments");
				const arma::field<Value>& arg1 = args(0);
				const arma::field<Value>& arg2 = args(1);

				// check matrix size
				if(arg1.n_rows!=arg2.n_rows || arg1.n_cols!=arg2.n_cols)
					rat_throw_line("for hypot arg1 and arg2 must be equal in size");

				// calculate output
				arma::field<Value> output(arg1.n_rows, arg1.n_cols);
				for(arma::uword i=0;i<arg1.n_elem;i++){
					// units must be equal
					if(arg1(i).get_unit()!=arg2(i).get_unit())
						rat_throw_line("hypot arguments must have equal units");

					// perform hypot calculation
					output(i) = Value(std::hypot(arg1(i).get_number(), arg2(i).get_number()),arg1(i).get_unit());
				}

				// return output
				return output;
			}},

			// initialize to zeros
			{"zeros", [](const  arma::field<arma::field<Value> >& args){
				// get individual arguments
				if(args.n_elem!=2)rat_throw_line("zeros requires exactly two arguments");
				const arma::field<Value>& arg1 = args(0);
				const arma::field<Value>& arg2 = args(1);

				// must be scalars
				if(arg1.n_elem!=1)rat_throw_line("zeros first dimension must be scalar");
				if(arg2.n_elem!=1)rat_throw_line("zeros second dimension must be scalar");

				// calculate output
				arma::field<Value> output(arma::uword(arg1(0).get_number()), arma::uword(arg2(0).get_number()));
				for(arma::uword i=0;i<output.n_elem;i++)
					output(i) = Value(0.0,Unit::D);

				// return output
				return output;
			}},

			// initialize to zeros
			{"ones", [](const  arma::field<arma::field<Value> >& args){
				// get individual arguments
				if(args.n_elem!=2)rat_throw_line("ones requires exactly two arguments");
				const arma::field<Value>& arg1 = args(0);
				const arma::field<Value>& arg2 = args(1);

				// must be scalars
				if(arg1.n_elem!=1)rat_throw_line("ones first dimension must be scalar");
				if(arg2.n_elem!=1)rat_throw_line("ones second dimension must be scalar");

				// calculate output
				arma::field<Value> output(arma::uword(arg1(0).get_number()), arma::uword(arg2(0).get_number()));
				for(arma::uword i=0;i<output.n_elem;i++)
					output(i) = Value(1.0,Unit::D);

				// return output
				return output;
			}},


			// 3 ARGUMENT functions
			// clamping
			{"clamp", [](const arma::field<arma::field<Value> >& args){
				// get individual arguments
				if(args.n_elem!=3)rat_throw_line("clamp requires exactly three arguments");
				const arma::field<Value>& arg1 = args(0);
				const arma::field<Value>& arg2 = args(1);
				const arma::field<Value>& arg3 = args(2);

				// check input
				if(arg2.n_elem!=1)rat_throw_line("clamp lower bound must be a scalar");
				if(arg3.n_elem!=1)rat_throw_line("clamp upper bound must be a scalar");
				
				// get scalar values
				const Value& val2 = arg2(0);
				const Value& val3 = arg3(0);

				// create output
				arma::field<Value> output(arg1.n_rows, arg1.n_cols);
				for(arma::uword i=0;i<arg1.n_elem;i++){
					if(arg1(i).get_unit()!=val2.get_unit())rat_throw_line("clamp units of lower bound do not match that of the input value");
					if(arg1(i).get_unit()!=val3.get_unit())rat_throw_line("clamp units of upper bound do not match that of the input value");
					output(i) = Value(std::min(std::max(arg1(i).get_number(),val2.get_number()),val3.get_number()), arg1(i).get_unit());
				}

				// return values
				return output;
			}},

			// linear space
			{"linspace", [](const arma::field<arma::field<Value> >& args){
				// get individual arguments
				if(args.n_elem!=3)rat_throw_line("linspace requires exactly three arguments");
				const arma::field<Value>& arg1 = args(0);
				const arma::field<Value>& arg2 = args(1);
				const arma::field<Value>& arg3 = args(2);
				
				// check input
				if(arg1.n_elem!=1)rat_throw_line("linspace lower bound must be a scalar");
				if(arg2.n_elem!=1)rat_throw_line("linspace upper bound must be a scalar");
				if(arg3.n_elem!=1)rat_throw_line("linspace number must be a scalar");
				
				// get values
				const Value& val1 = arg1(0);
				const Value& val2 = arg2(0);
				const Value& val3 = arg3(0);
				
				// check units
				if(val1.get_unit()!=val2.get_unit())
					rat_throw_line("linspace units of lower and upper bounds must be equal");
				
				if(val3.get_unit()!=Unit::D)
					rat_throw_line("linspace number of values argument must be unitless");
				
				// get number of values
				const arma::uword num_val = arma::uword(std::floor(val3.get_number()));

				// create output values
				arma::field<Value> output;
				if(num_val==0)
					return output.set_size(1,0);
				else if(num_val==1){
					output.set_size(1,1);
					output(0) = Value(val2.get_number(), val1.get_unit());
				}else{
					output.set_size(1,num_val);
					for(arma::uword i=0;i<num_val;i++)
						output(i) = Value(val1.get_number() + 
							i*(val2.get_number() - val1.get_number())/
							(num_val-1), val1.get_unit());
				}

				// output values
				return output;
			}},

			// regular space
			{"regspace", [](const arma::field<arma::field<Value> >& args){
				// get individual arguments
				if(args.n_elem!=3)rat_throw_line("regspace requires exactly three arguments");
				const arma::field<Value>& arg1 = args(0);
				const arma::field<Value>& arg2 = args(1);
				const arma::field<Value>& arg3 = args(2);
				
				// check input
				if(arg1.n_elem!=1)rat_throw_line("regspace lower bound must be a scalar");
				if(arg2.n_elem!=1)rat_throw_line("regspace upper bound must be a scalar");
				if(arg3.n_elem!=1)rat_throw_line("regspace number must be a scalar");
				
				// get values
				const Value& val1 = arg1(0);
				const Value& val2 = arg2(0);
				const Value& val3 = arg3(0);
				
				// check units
				if(val2.get_unit()!=val1.get_unit())
					rat_throw_line("regspace units of step must be equal to lower bound");
				if(val2.get_unit()!=val3.get_unit())
					rat_throw_line("regspace units of step must be equal to upper bound");
			
				// get number of values
				const arma::uword num_val = arma::uword(std::floor((val3.get_number() - val1.get_number())/val2.get_number())) + 1;

				// create output values
				arma::field<Value> output;
				if(num_val==0)
					return output.set_size(1,0);
				else if(num_val==1){
					output.set_size(1,1);
					output(0) = Value(val1.get_number(), val1.get_unit());
				}else{
					output.set_size(1,num_val);
					for(arma::uword i=0;i<num_val;i++)
						output(i) = Value(val1.get_number() + 
							i*val2.get_number(), val1.get_unit());
				}

				// output values
				return output;
			}},

			// MULTIARG functions
			// extremes (need to add more than one input options here)
			{"min", [](const  arma::field<arma::field<Value> >& args){
				// get input
				if (args.n_elem != 1) rat_throw_line("min requires exactly one argument");
				const arma::field<Value>& arg = args(0);

				// no value case
				if(arg.n_elem==0)return arma::field<Value>{};
				
				// find smallest value
				fltp minval=arma::Datum<fltp>::inf;
				for(arma::uword i=0;i<arg.n_elem;i++){
					if(arg(i).get_unit()!=arg(0).get_unit())
						rat_throw_line("min requires all input units to be the same");
					minval = std::min(minval, arg(i).get_number());
				}

				// create scalar and return
				return arma::field<Value>{Value(minval, arg(0).get_unit())};
			}},

			// extremes
			{"max", [](const  arma::field<arma::field<Value> >& args){
				// get input
				if (args.n_elem != 1) rat_throw_line("max requires exactly one argument");
				const arma::field<Value>& arg = args(0);

				// no value case
				if(arg.n_elem==0)return arma::field<Value>{};
				
				// find smallest value
				fltp maxval=-arma::Datum<fltp>::inf;
				for(arma::uword i=0;i<arg.n_elem;i++){
					if(arg(i).get_unit()!=arg(0).get_unit())
						rat_throw_line("max requires all input units to be the same");
					maxval = std::max(maxval, arg(i).get_number());
				}

				// create scalar and return
				return arma::field<Value>{Value(maxval, arg(0).get_unit())};
			}},


			// TODO modulus

			// ... Any other functions ...
		};
	}
	

	void Parser::load_default_unit_table(){
		// unit table
		unit_table_ = std::map<std::string, Value>{
			// Time
			{"hour",Value(3600.0, Unit::s)},
			{"min", Value(60.0, Unit::s)},
			{"ks", Value(1E+3, Unit::s)},
			{"s", Value(1.0, Unit::s)},
			{"ms", Value(1E-3, Unit::s)},
			{"us", Value(1E-6, Unit::s)},
			{"ns", Value(1E-9, Unit::s)},

			// Distance
			{"km", Value(1E+3, Unit::m)},
			{"m", Value(1.0, Unit::m)},
			{"dm", Value(1E-1, Unit::m)},
			{"cm", Value(1E-2, Unit::m)},
			{"mm", Value(1E-3, Unit::m)},
			{"um", Value(1E-6, Unit::m)},
			{"nm", Value(1E-9, Unit::m)},

			// Mass
			{"t", Value(1E+3, Unit::kg)},
			{"kg", Value(1.0, Unit::kg)},
			{"g", Value(1E-3, Unit::kg)},
			{"mg", Value(1E-6, Unit::kg)},
			{"ug", Value(1E-9, Unit::kg)},

			// Current
			{"MA", Value(1E+6, Unit::A)},
			{"kA", Value(1E+3, Unit::A)},
			{"A", Value(1.0, Unit::A)},
			{"mA", Value(1E-3, Unit::A)},
			{"uA", Value(1E-6, Unit::A)},

			// Amount
			{"kmol", Value(1E+3, Unit::mol)},
			{"mol", Value(1.0, Unit::mol)},
			{"mmol", Value(1E-3, Unit::mol)},
			{"umol", Value(1E-6, Unit::mol)},

			// Temperature
			{"RT", Value(300.0, Unit::K)},
			{"K", Value(1.0, Unit::K)},
			{"mK", Value(1E-3, Unit::K)},
			{"uK", Value(1E-6, Unit::K)},

			// Candela
			{"cd", Value(1.0, Unit::cd)},

			// Energy
			{"muJ", Value(1e-6, Unit::kg*(Unit::m^2)/(Unit::s^2))},
			{"mJ", Value(1e-3, Unit::kg*(Unit::m^2)/(Unit::s^2))},
			{"J", Value(1.0, Unit::kg*(Unit::m^2)/(Unit::s^2))},
			{"kJ", Value(1e3, Unit::kg*(Unit::m^2)/(Unit::s^2))},
			{"MJ", Value(1e6, Unit::kg*(Unit::m^2)/(Unit::s^2))},
			{"GJ", Value(1e9, Unit::kg*(Unit::m^2)/(Unit::s^2))},
			{"erg", Value(1E-7, Unit::kg*(Unit::m^2)/(Unit::s^2))}, 
			{"eV", Value(1.602176634E-19, Unit::kg*(Unit::m^2)/(Unit::s^2))},
			{"kcal", Value(4184.0, Unit::kg*(Unit::m^2)/(Unit::s^2))},

			// Power
			{"mW", Value(1E-3, Unit::kg*(Unit::m^2)/(Unit::s^3))},
			{"W", Value(1.0, Unit::kg*(Unit::m^2)/(Unit::s^3))},
			{"kW", Value(1E3, Unit::kg*(Unit::m^2)/(Unit::s^3))},
			{"MW", Value(1E6, Unit::kg*(Unit::m^2)/(Unit::s^3))},

			// Force
			{"mN", Value(1E-3, Unit::kg*Unit::m/(Unit::s^2))}, 
			{"N", Value(1.0, Unit::kg*Unit::m/(Unit::s^2))}, 
			{"kN", Value(1E3, Unit::kg*Unit::m/(Unit::s^2))},
			{"MN", Value(1E6, Unit::kg*Unit::m/(Unit::s^2))},
			{"dyn", Value(1E-5, Unit::kg*Unit::m/(Unit::s^2))},

			// Voltage
			{"nV", Value(1E-9, Unit::kg*(Unit::m^2)/(Unit::s^3)/Unit::A)},
			{"uV", Value(1E-6, Unit::kg*(Unit::m^2)/(Unit::s^3)/Unit::A)},
			{"mV", Value(1E-3, Unit::kg*(Unit::m^2)/(Unit::s^3)/Unit::A)},
			{"V", Value(1.0, Unit::kg*(Unit::m^2)/(Unit::s^3)/Unit::A)},
			{"kV", Value(1E+3, Unit::kg*(Unit::m^2)/(Unit::s^3)/Unit::A)},

			// Resistance
			{"nOhm", Value(1E-9, Unit::kg*(Unit::m^2)/(Unit::s^3)/(Unit::A^2))},
			{"uOhm", Value(1E-6, Unit::kg*(Unit::m^2)/(Unit::s^3)/(Unit::A^2))},
			{"mOhm", Value(1E-3, Unit::kg*(Unit::m^2)/(Unit::s^3)/(Unit::A^2))},
			{"Ohm", Value(1.0, Unit::kg*(Unit::m^2)/(Unit::s^3)/(Unit::A^2))},
			{"kOhm", Value(1E3, Unit::kg*(Unit::m^2)/(Unit::s^3)/(Unit::A^2))},

			// Inductance
			{"uH", Value(1E-6, Unit::kg*(Unit::m^2)/(Unit::s^2)/(Unit::A^2))},
			{"mH", Value(1E-3, Unit::kg*(Unit::m^2)/(Unit::s^2)/(Unit::A^2))},
			{"H", Value(1.0, Unit::kg*(Unit::m^2)/(Unit::s^2)/(Unit::A^2))},

			// Capacitance
			{"pF", Value(1E-12, (Unit::s^4)*(Unit::A^2)/(Unit::m^2)/Unit::kg)},
			{"nF", Value(1E-9, (Unit::s^4)*(Unit::A^2)/(Unit::m^2)/Unit::kg)},
			{"uF", Value(1E-6, (Unit::s^4)*(Unit::A^2)/(Unit::m^2)/Unit::kg)},
			{"mF", Value(1E-3, (Unit::s^4)*(Unit::A^2)/(Unit::m^2)/Unit::kg)},
			{"F", Value(1.0, (Unit::s^4)*(Unit::A^2)/(Unit::m^2)/Unit::kg)},

			// Magnetic field
			{"uT", Value(1E-6, Unit::kg/(Unit::s^2)/Unit::A)},
			{"mT", Value(1E-3, Unit::kg/(Unit::s^2)/Unit::A)},
			{"T", Value(1.0, Unit::kg/(Unit::s^2)/Unit::A)},
			{"kT", Value(1E3, Unit::kg/(Unit::s^2)/Unit::A)},

			// Frequency
			{"GHz", Value(1E+9, Unit::D/Unit::s)},
			{"MHz", Value(1E+6, Unit::D/Unit::s)},
			{"kHz", Value(1E+3, Unit::D/Unit::s)},
			{"Hz", Value(1E+0, Unit::D/Unit::s)},
			{"mHz", Value(1E-3, Unit::D/Unit::s)},
			{"uHz", Value(1E-6, Unit::D/Unit::s)},
			{"Bq", Value(1E+0, Unit::D/Unit::s)},

			// Pressure
			{"Torr", Value(133.322, Unit::kg/Unit::m/(Unit::s^2))},
			{"atm", Value(101325, Unit::kg/Unit::m/(Unit::s^2))},
			{"Pa", Value(1.0, Unit::kg/Unit::m/(Unit::s^2))},
			{"kbar", Value(1E+8, Unit::kg/Unit::m/(Unit::s^2))},
			{"bar", Value(1E+5, Unit::kg/Unit::m/(Unit::s^2))},
			{"mbar", Value(1E+2, Unit::kg/Unit::m/(Unit::s^2))},

			// Electrical charge
			{"C", Value(1.0, Unit::A*Unit::s)},  //Coulomb (Charge)

			// Physical Constants
			{"R", Value(8.31446261815324, Unit::kg*(Unit::m^2)/(Unit::s^2)/Unit::K/Unit::mol)},
			{"kB", Value(1.380649E-23, Unit::kg*(Unit::m^2)/(Unit::s^2)/Unit::K)},
			{"F", Value(96485.3329, Unit::A*Unit::s/Unit::mol)},
			{"NA", Value(6.02214076E+23, Unit::D/Unit::mol)},
			{"h", Value(6.62607015E-34, Unit::kg*(Unit::m^2)/(Unit::s^3))},

			// Mathematical Constants
			{"pi", Value(arma::Datum<fltp>::pi, Unit::D)},
			{"e", Value(arma::Datum<fltp>::e, Unit::D)},

			// Angular unit
			{"rad", Value(1.0, Unit::D)},
			{"deg", Value(arma::Datum<fltp>::pi/180, Unit::D)},

			// Imperial Length Units
			{"in", Value(0.0254, Unit::m)}, // Inch
			{"ft", Value(0.3048, Unit::m)}, // Foot
			{"yd", Value(0.9144, Unit::m)}, // Yard
			{"mi", Value(1609.34, Unit::m)}, // Mile

			// Imperial Area Units
			{"acre", Value(4046.86, Unit::m^2)}, // Acre

			// Imperial Mass Units
			{"lb", Value(0.453592, Unit::kg)}, // Pound
			{"oz", Value(0.0283495, Unit::kg)}, // Ounce

			// Imperial Pressure Units
			{"psi", Value(6894.76, Unit::kg/Unit::m/(Unit::s^2))} // Pounds per square inch
		};
	}

	// add list of operators
	void Parser::load_default_operators(){
		// dont forget to also add to precedence function
		operators_ = std::map<std::string, std::function<arma::field<Value>(const arma::field<Value>&, const arma::field<Value>&)> >{
			{"+", [](const arma::field<Value>& operand1, const arma::field<Value>& operand2){
				// allocate output
				arma::field<Value> output;

				// matrix multiplication
				if(operand1.n_cols==operand2.n_cols && operand1.n_rows==operand2.n_rows){
					// create output
					output.set_size(operand1.n_rows, operand1.n_cols);

					// Perform matrix multiplication
					for (arma::uword i = 0; i < operand1.n_rows; ++i) {
						for (arma::uword j = 0; j < operand2.n_cols; ++j) {
							output(i,j) = operand1(i,j) + operand2(i,j);
						}
					}
				}

				// scalar times matrix
				else if(operand1.n_elem==1){
					output.set_size(operand2.n_rows, operand2.n_cols);
					for(arma::uword i=0;i<operand2.n_elem;i++){
						output(i) = operand1(0) + operand2(i);
					}
				}

				// matrix times scalar
				else if(operand2.n_elem==1){
					output.set_size(operand1.n_rows, operand1.n_cols);
					for(arma::uword i=0;i<operand1.n_elem;i++){
						output(i) = operand1(i) + operand2(0);
					}
				}

				// otherwise
				else{
					rat_throw_line("addition not possible due to invalid operand sizes");
				}


				// done
				return output;
			}},

			{"-", [](const arma::field<Value>& operand1, const arma::field<Value>& operand2){
				// allocate output
				arma::field<Value> output;

				// matrix multiplication
				if(operand1.n_cols==operand2.n_cols && operand1.n_rows==operand2.n_rows){
					// create output
					output.set_size(operand1.n_rows, operand1.n_cols);

					// Perform matrix multiplication
					for (arma::uword i = 0; i < operand1.n_rows; ++i) {
						for (arma::uword j = 0; j < operand2.n_cols; ++j) {
							output(i,j) = operand1(i,j) - operand2(i,j);
						}
					}
				}

				// scalar times matrix
				else if(operand1.n_elem==1){
					output.set_size(operand2.n_rows, operand2.n_cols);
					for(arma::uword i=0;i<operand2.n_elem;i++){
						output(i) = operand1(0) - operand2(i);
					}
				}

				// matrix times scalar
				else if(operand2.n_elem==1){
					output.set_size(operand1.n_rows, operand1.n_cols);
					for(arma::uword i=0;i<operand1.n_elem;i++){
						output(i) = operand1(i) - operand2(0);
					}
				}

				// otherwise
				else{
					rat_throw_line("addition not possible due to invalid operand sizes");
				}


				// done
				return output;
			}},

			// // matrix multiplication
			// {"*", [](const arma::field<Value>& operand1, const arma::field<Value>& operand2){
			// 	// allocate output
			// 	arma::field<Value> output;

			// 	// matrix multiplication
			// 	if(operand1.n_cols==operand2.n_rows){
			// 		// create output
			// 		output.set_size(operand1.n_rows, operand2.n_cols);

			// 		// Perform matrix multiplication
			// 	    for (arma::uword i = 0; i < operand1.n_rows; ++i) {
			// 	        for (arma::uword j = 0; j < operand2.n_cols; ++j) {
			// 	        	output(i,j)=0.0;
			// 	            for (arma::uword k = 0; k < operand1.n_cols; ++k) {
			// 	                output(i,j) = output(i,j) + operand1(i,k) * operand2(k,j);
			// 	            }
			// 	        }
			// 	    }
			// 	}

			// 	// scalar times matrix
			// 	else if(operand1.n_elem==1){
			// 		output.set_size(operand2.n_rows, operand2.n_cols);
			// 		for(arma::uword i=0;i<operand2.n_elem;i++){
			// 			output(i) = operand1(0)*operand2(i);
			// 		}
			// 	}

			// 	// matrix times scalar
			// 	else if(operand2.n_elem==1){
			// 		output.set_size(operand1.n_rows, operand1.n_cols);
			// 		for(arma::uword i=0;i<operand1.n_elem;i++){
			// 			output(i) = operand1(i)*operand2(0);
			// 		}
			// 	}

			// 	// otherwise
			// 	else{
			// 		rat_throw_line("matrix multiplication not possible due to invalid operand sizes");
			// 	}

			// 	// done
			// 	return output;
			// }},

			// element wise division
			{"/", [](const arma::field<Value>& operand1, const arma::field<Value>& operand2){
				// allocate output
				arma::field<Value> output;

				// std::cout<<"Mult"<<std::endl;
				// std::cout<<operand1.n_rows<<" "<<operand2.n_rows<<std::endl;
				// std::cout<<operand1.n_cols<<" "<<operand2.n_cols<<std::endl;

				// element wise division
				if(operand1.n_cols==operand2.n_cols && operand1.n_rows==operand2.n_rows){
					// create output
					output.set_size(operand1.n_rows, operand1.n_cols);

					// Perform matrix multiplication
					for(arma::uword i=0;i<operand1.n_elem;i++){
						output(i) = operand1(i)/operand2(i);
					}
				}

				// scalar times matrix
				else if(operand1.n_elem==1){
					output.set_size(operand2.n_rows, operand2.n_cols);
					for(arma::uword i=0;i<operand2.n_elem;i++){
						output(i) = operand1(0)/operand2(i);
					}
				}

				// matrix times scalar
				else if(operand2.n_elem==1){
					output.set_size(operand1.n_rows, operand1.n_cols);
					for(arma::uword i=0;i<operand1.n_elem;i++){
						output(i) = operand1(i)/operand2(0);
					}
				}

				// otherwise
				else{
					rat_throw_line("division not possible due to invalid operand sizes");
				}

				// done
				return output;
			}},

			{"$", [](const arma::field<Value>& operand1, const arma::field<Value>& operand2){
				// allocate output
				arma::field<Value> output;

				// element wise division
				if(operand1.n_cols==operand2.n_cols && operand1.n_rows==operand2.n_rows){
					// create output
					output.set_size(operand1.n_rows, operand1.n_cols);

					// Perform matrix multiplication
					for(arma::uword i=0;i<operand1.n_elem;i++){
						output(i) = operand1(i)*operand2(i);
					}
				}

				// scalar times matrix
				else if(operand1.n_elem==1){
					output.set_size(operand2.n_rows, operand2.n_cols);
					for(arma::uword i=0;i<operand2.n_elem;i++){
						output(i) = operand1(0)*operand2(i);
					}
				}

				// matrix times scalar
				else if(operand2.n_elem==1){
					output.set_size(operand1.n_rows, operand1.n_cols);
					for(arma::uword i=0;i<operand1.n_elem;i++){
						output(i) = operand1(i)*operand2(0);
					}
				}

				// otherwise
				else{
					rat_throw_line("multiplication not possible due to invalid operand sizes");
				}

				// done
				return output;
			}},

			{"*", [](const arma::field<Value>& operand1, const arma::field<Value>& operand2){
				// allocate output
				arma::field<Value> output;

				// matrix multiplication
				if(operand1.n_cols==operand2.n_cols && operand1.n_rows==operand2.n_rows){
					// create output
					output.set_size(operand1.n_rows, operand1.n_cols);

					// Perform matrix multiplication
					for(arma::uword i=0;i<operand1.n_elem;i++) {
						output(i) = operand1(i)*operand2(i);
					}
				}

				// scalar times matrix
				else if(operand1.n_elem==1){
					output.set_size(operand2.n_rows, operand2.n_cols);
					for(arma::uword i=0;i<operand2.n_elem;i++){
						output(i) = operand1(0)*operand2(i);
					}
				}

				// matrix times scalar
				else if(operand2.n_elem==1){
					output.set_size(operand1.n_rows, operand1.n_cols);
					for(arma::uword i=0;i<operand1.n_elem;i++){
						output(i) = operand1(i)*operand2(0);
					}
				}

				// otherwise
				else{
					rat_throw_line("element wise multiplication not possible due to invalid operand sizes");
				}


				// done
				return output;
			}},


			// element wise power
			{"^", [](const arma::field<Value>& operand1, const arma::field<Value>& operand2){
				// allocate output
				arma::field<Value> output;

				// matrix multiplication
				if(operand1.n_cols==operand2.n_cols && operand1.n_rows==operand2.n_rows){
					// create output
					output.set_size(operand1.n_rows, operand1.n_cols);

					// perform power calculation
					for(arma::uword i=0;i<operand1.n_elem;i++){
						output(i) = operand1(i)^operand2(i);
					}
				}

				// scalar times matrix
				else if(operand1.n_elem==1){
					output.set_size(operand2.n_rows, operand2.n_cols);
					for(arma::uword i=0;i<operand2.n_elem;i++){
						output(i) = operand1(0)^operand2(i);
					}
				}

				// matrix times scalar
				else if(operand2.n_elem==1){
					output.set_size(operand1.n_rows, operand1.n_cols);
					for(arma::uword i=0;i<operand1.n_elem;i++){
						output(i) = operand1(i)^operand2(0);
					}
				}

				// otherwise
				else{
					rat_throw_line("addition not possible due to invalid operand sizes");
				}


				// done
				return output;
			}},
				
			// element wise equality
			{"==", [](const arma::field<Value>& operand1, const arma::field<Value>& operand2){
				// allocate output
				arma::field<Value> output;

				// matrix multiplication
				if(operand1.n_cols==operand2.n_cols && operand1.n_rows==operand2.n_rows){
					// create output
					output.set_size(operand1.n_rows, operand1.n_cols);

					// Perform matrix multiplication
					for (arma::uword i = 0; i < operand1.n_rows; ++i) {
						for (arma::uword j = 0; j < operand2.n_cols; ++j) {
							output(i,j) = Value(fltp(operand1(i,j)==operand2(i,j)),Unit::D);
						}
					}
				}

				// scalar times matrix
				else if(operand1.n_elem==1){
					output.set_size(operand2.n_rows, operand2.n_cols);
					for(arma::uword i=0;i<operand2.n_elem;i++){
						output(i) = Value(fltp(operand1(0)==operand2(i)),Unit::D);
					}
				}

				// matrix times scalar
				else if(operand2.n_elem==1){
					output.set_size(operand1.n_rows, operand1.n_cols);
					for(arma::uword i=0;i<operand1.n_elem;i++){
						output(i) = Value(fltp(operand1(i)==operand2(0)),Unit::D);
					}
				}

				// otherwise
				else{
					rat_throw_line("addition not possible due to invalid operand sizes");
				}


				// done
				return output;
			}},

			// element wise inequality
			{"!=", [](const arma::field<Value>& operand1, const arma::field<Value>& operand2){
				// allocate output
				arma::field<Value> output;

				// matrix multiplication
				if(operand1.n_cols==operand2.n_cols && operand1.n_rows==operand2.n_rows){
					// create output
					output.set_size(operand1.n_rows, operand1.n_cols);

					// Perform matrix multiplication
					for (arma::uword i = 0; i < operand1.n_rows; ++i) {
						for (arma::uword j = 0; j < operand2.n_cols; ++j) {
							output(i,j) = Value(fltp(operand1(i,j)!=operand2(i,j)),Unit::D);
						}
					}
				}

				// scalar times matrix
				else if(operand1.n_elem==1){
					output.set_size(operand2.n_rows, operand2.n_cols);
					for(arma::uword i=0;i<operand2.n_elem;i++){
						output(i) = Value(fltp(operand1(0)!=operand2(i)),Unit::D);
					}
				}

				// matrix times scalar
				else if(operand2.n_elem==1){
					output.set_size(operand1.n_rows, operand1.n_cols);
					for(arma::uword i=0;i<operand1.n_elem;i++){
						output(i) = Value(fltp(operand1(i)!=operand2(0)),Unit::D);
					}
				}

				// otherwise
				else{
					rat_throw_line("addition not possible due to invalid operand sizes");
				}


				// done
				return output;
			}},

			// element wise comparison
			{">", [](const arma::field<Value>& operand1, const arma::field<Value>& operand2){
				// allocate output
				arma::field<Value> output;

				// matrix multiplication
				if(operand1.n_cols==operand2.n_cols && operand1.n_rows==operand2.n_rows){
					// create output
					output.set_size(operand1.n_rows, operand1.n_cols);

					// Perform matrix multiplication
					for (arma::uword i = 0; i < operand1.n_rows; ++i) {
						for (arma::uword j = 0; j < operand2.n_cols; ++j) {
							output(i,j) = Value(fltp(operand1(i,j)>operand2(i,j)),Unit::D);
						}
					}
				}

				// scalar times matrix
				else if(operand1.n_elem==1){
					output.set_size(operand2.n_rows, operand2.n_cols);
					for(arma::uword i=0;i<operand2.n_elem;i++){
						output(i) = Value(fltp(operand1(0)>operand2(i)),Unit::D);
					}
				}

				// matrix times scalar
				else if(operand2.n_elem==1){
					output.set_size(operand1.n_rows, operand1.n_cols);
					for(arma::uword i=0;i<operand1.n_elem;i++){
						output(i) = Value(fltp(operand1(i)>operand2(0)),Unit::D);
					}
				}

				// otherwise
				else{
					rat_throw_line("addition not possible due to invalid operand sizes");
				}


				// done
				return output;
			}},

			// element wise comparison
			{"<", [](const arma::field<Value>& operand1, const arma::field<Value>& operand2){
				// allocate output
				arma::field<Value> output;

				// matrix multiplication
				if(operand1.n_cols==operand2.n_cols && operand1.n_rows==operand2.n_rows){
					// create output
					output.set_size(operand1.n_rows, operand1.n_cols);

					// Perform matrix multiplication
					for (arma::uword i = 0; i < operand1.n_rows; ++i) {
						for (arma::uword j = 0; j < operand2.n_cols; ++j) {
							output(i,j) = Value(fltp(operand1(i,j)<operand2(i,j)),Unit::D);
						}
					}
				}

				// scalar times matrix
				else if(operand1.n_elem==1){
					output.set_size(operand2.n_rows, operand2.n_cols);
					for(arma::uword i=0;i<operand2.n_elem;i++){
						output(i) = Value(fltp(operand1(0)<operand2(i)),Unit::D);
					}
				}

				// matrix times scalar
				else if(operand2.n_elem==1){
					output.set_size(operand1.n_rows, operand1.n_cols);
					for(arma::uword i=0;i<operand1.n_elem;i++){
						output(i) = Value(fltp(operand1(i)<operand2(0)),Unit::D);
					}
				}

				// otherwise
				else{
					rat_throw_line("addition not possible due to invalid operand sizes");
				}


				// done
				return output;
			}},

			// element wise comparison
			{">=", [](const arma::field<Value>& operand1, const arma::field<Value>& operand2){
				// allocate output
				arma::field<Value> output;

				// matrix multiplication
				if(operand1.n_cols==operand2.n_cols && operand1.n_rows==operand2.n_rows){
					// create output
					output.set_size(operand1.n_rows, operand1.n_cols);

					// Perform matrix multiplication
					for (arma::uword i = 0; i < operand1.n_rows; ++i) {
						for (arma::uword j = 0; j < operand2.n_cols; ++j) {
							output(i,j) = Value(fltp(operand1(i,j)>=operand2(i,j)),Unit::D);
						}
					}
				}

				// scalar times matrix
				else if(operand1.n_elem==1){
					output.set_size(operand2.n_rows, operand2.n_cols);
					for(arma::uword i=0;i<operand2.n_elem;i++){
						output(i) = Value(fltp(operand1(0)>=operand2(i)),Unit::D);
					}
				}

				// matrix times scalar
				else if(operand2.n_elem==1){
					output.set_size(operand1.n_rows, operand1.n_cols);
					for(arma::uword i=0;i<operand1.n_elem;i++){
						output(i) = Value(fltp(operand1(i)>=operand2(0)),Unit::D);
					}
				}

				// otherwise
				else{
					rat_throw_line("addition not possible due to invalid operand sizes");
				}


				// done
				return output;
			}},

			// element wise comparison
			{"<=", [](const arma::field<Value>& operand1, const arma::field<Value>& operand2){
				// allocate output
				arma::field<Value> output;

				// matrix multiplication
				if(operand1.n_cols==operand2.n_cols && operand1.n_rows==operand2.n_rows){
					// create output
					output.set_size(operand1.n_rows, operand1.n_cols);

					// Perform matrix multiplication
					for (arma::uword i = 0; i < operand1.n_rows; ++i) {
						for (arma::uword j = 0; j < operand2.n_cols; ++j) {
							output(i,j) = Value(operand1(i,j)<=operand2(i,j),Unit::D);
						}
					}
				}

				// scalar times matrix
				else if(operand1.n_elem==1){
					output.set_size(operand2.n_rows, operand2.n_cols);
					for(arma::uword i=0;i<operand2.n_elem;i++){
						output(i) = Value(operand1(0)<=operand2(i),Unit::D);
					}
				}

				// matrix times scalar
				else if(operand2.n_elem==1){
					output.set_size(operand1.n_rows, operand1.n_cols);
					for(arma::uword i=0;i<operand1.n_elem;i++){
						output(i) = Value(operand1(i)<=operand2(0),Unit::D);
					}
				}

				// otherwise
				else{
					rat_throw_line("addition not possible due to invalid operand sizes");
				}


				// done
				return output;
			}},

			{",", [](const arma::field<Value>& operand1, const arma::field<Value>& operand2){
				// Combines two vectors into one
				if(operand1.n_rows!=operand2.n_rows)rat_throw_line("operands must have same number of rows");
				arma::field<Value> combined(operand1.n_rows, operand1.n_cols + operand2.n_cols);
				for(arma::uword i=0;i<operand1.n_cols;i++)
					combined.col(i) = operand1.col(i);
				for(arma::uword i=0;i<operand2.n_cols;i++)
					combined.col(operand1.n_cols + i) = operand2.col(i);
				return combined;
			}},

			{";", [](const arma::field<Value>& operand1, const arma::field<Value>& operand2){
				// Combines two vectors into one
				if(operand1.n_cols!=operand2.n_cols)rat_throw_line("operands must have same number of rows");
				arma::field<Value> combined(operand1.n_rows + operand2.n_rows, operand1.n_cols);
				for(arma::uword i=0;i<operand1.n_rows;i++)
					combined.row(i) = operand1.row(i);
				for(arma::uword i=0;i<operand2.n_rows;i++)
					combined.row(operand1.n_rows + i) = operand2.row(i);
				return combined;
			}},
		};



	}


	// add variables
	void Parser::set_variable(const std::string& var, const Equation& eq){
		if(fnmap_.find(var)!=fnmap_.end())rat_throw_line("variable is also a function name: " + var);
		if(unit_table_.find(var)!=unit_table_.end())rat_throw_line("variable is also a unit: " + var);
		variables_[var] = eq;
	}

	// run evaluation
	arma::Mat<fltp> Parser::evaluate(const std::string& expression, const Unit& unit)const{
		arma::field<Value> values = evaluate(expression);
		arma::Mat<fltp> output(values.n_rows, values.n_cols);
		for(arma::uword i=0;i<values.n_elem;i++){
			if(values(i).get_unit()!=unit && values(i).get_number()!=0)
				rat_throw_line("value does not match requested unit: " + 
					values(i).get_unit().as_string() + " instead of "+ unit.as_string());
			output(i) = values(i).get_number();
		}
		return output;
	}

	// run evaluation
	arma::field<Value> Parser::evaluate(const std::string& expression)const{
		if(expression.empty())rat_throw_line("expression is empty");
		std::string str = expression;
		str.erase(std::remove(str.begin(), str.end(), ' '), str.end());
		const std::list<std::string> tokens = tokenize(str);
		// check_brackets(tokens); // this is now done in infix to rpn
		const std::list<std::string> rpn = infix_to_rpn(tokens);
		const arma::field<Value> values = evaluate_rpn(rpn);
		return values;
	}

	// run evaluation
	Value Parser::evaluate_to_scalar(const std::string& expression)const{
		const arma::field<Value> values = evaluate(expression);
		if(values.n_elem>1)rat_throw_line("output is not scalar");
		return values(0);
	}

	// run evaluation
	arma::Mat<fltp> Parser::evaluate(const Equation& eq, const Unit& unit)const{
		arma::field<Value> values = evaluate(eq);
		arma::Mat<fltp> output(values.n_rows, values.n_cols);
		for(arma::uword i=0;i<values.n_elem;i++){
			if(values(i).get_unit()!=unit && values(i).get_number()!=0)
				rat_throw_line("value does not match requested unit: " + 
				values(i).get_unit().as_string() + " instead of "+ unit.as_string());
			output(i) = values(i).get_number();
		}
		return output;
	}


	// run evaluation
	arma::field<Value> Parser::evaluate(const Equation& eq)const{
		return evaluate(eq.get_expression());
	}

	// run evaluation
	Value Parser::evaluate_to_scalar(const Equation& eq)const{
		const arma::field<Value> values = evaluate(eq.get_expression());
		if(values.n_elem>1)rat_throw_line("output is not scalar");
		return values(0);
	}

	fltp Parser::evaluate_to_scalar(const Equation& eq, const Unit& unit)const{
		const Value value = evaluate_to_scalar(eq);
		if(value.get_unit()!=unit && value.get_number()!=0){
			rat_throw_line("value does not match requested unit: " + 
				value.get_unit().as_string() + " instead of "+ unit.as_string());
		}
		return value.get_number();
	}

	// calc derivative
	Equation Parser::calc_derivative(const Equation& eq, const std::string& variable)const{
		return Equation(calc_derivative(eq.get_expression(),variable));
	}

	// calculate derivative
	std::string Parser::calc_derivative(
		const std::string& expression, 
		const std::string& towards_variable)const{

		// check input
		if(expression.empty())rat_throw_line("expression is empty");

		// check if the towards 
		if(!is_variable(towards_variable))rat_throw_line("variable does not exist");

		// remove spaces
		std::string str = expression;
		str.erase(std::remove(str.begin(), str.end(), ' '), str.end());
		
		// convert to tokens
		const std::list<std::string> tokens = tokenize(str);
		check_brackets(tokens);

		// convert to reverse polish notation
		const std::list<std::string> rpn = infix_to_rpn(tokens);

		// convert to AST tree
		const ShASTNodePr tree = rpn_to_ast(rpn)->simplify();

		// calculate drivative from ast tree
		// const ShASTNodePr derivative = ASTOpNode::create(
		//     "/",tree->calculate_derivative(towards_variable),
		//     ASTFnNode::create("unit",
		//     ASTNode::create(towards_variable)));

		// calculate drivative from ast tree
		const ShASTNodePr derivative = tree->calculate_derivative(towards_variable);

		// simplify derivative tree
		const arma::uword max_iter = 20;
		ShASTNodePr derivative_simplified1 = derivative->simplify();
		for(arma::uword i=0;i<max_iter;i++){
			const ShASTNodePr derivative_simplified2 = derivative_simplified1->simplify();
			if(derivative_simplified1->is_equal(derivative_simplified2))break;
			derivative_simplified1 = derivative_simplified2;
		}

		// convert back to RPN 
		const std::list<std::string> rpn_derivative = 
			derivative_simplified1->to_rpn_notation();

		// convert back to equation and return
		return rpn_to_infix(rpn_derivative);
	}

	// construct value from string
	Value Parser::construct_value(const std::string& token) const {
		// default values
		fltp num = RAT_CONST(1.0); // Default value
		Unit unit = Unit::D; // Default unit


		// Find the position where the numeric part ends and unit part begins
		size_t pos = 0;
		bool eEncountered = false;  // Flag to track if 'e' or 'E' has been encountered
	   
		// first char must be a digit or a .
		if(std::isdigit(token[pos]) || token[pos] == '.'){
			while (pos < token.length()) {
				if(std::isdigit(token[pos]) || token[pos] == '.' || 
					((token[pos] == 'e' || token[pos] == 'E') && !eEncountered)) {
					if (token[pos] == 'e' || token[pos] == 'E') {
						eEncountered = true;
						// Include the sign after 'e' or 'E', if present
						if (pos + 1 < token.length() && (token[pos + 1] == '+' || token[pos + 1] == '-')) {
							pos++;
						}
					}
				}else if(eEncountered || token[pos] != '+' || token[pos] != '-') {
					// Break if any other non-digit character is encountered after 'e' or 'E'
					break;
				}
				pos++;
			}
		}

		// Extract the numeric part and the unit part
		std::string numStr = token.substr(0, pos);
		std::string unitStr = token.substr(pos);

		if (!numStr.empty()) {
			std::istringstream numStream(numStr);
			numStream >> num; // Convert the numeric string to a double
		}

		if (!unitStr.empty()) {
			auto it = unit_table_.find(unitStr);
			if (it == unit_table_.end()) {
				rat_throw_line("token not recognized: " + unitStr);
			}
			unit = it->second.get_unit();
			num = num * it->second.get_number();
		}
		return Value(num, unit);
	}


	void Parser::check_brackets(const std::list<std::string>& tokens){
		std::stack<char> bracket_stack;

		for (const auto& token : tokens) {
			if (token == "(" || token == "[") {
				// Push opening brackets onto the stack
				bracket_stack.push(token[0]);
			} else if (token == ")" || token == "]") {
				// Check for a matching opening bracket
				if (bracket_stack.empty()) {
					rat_throw_line("unmatched closing bracket");
				}

				char top = bracket_stack.top();
				bracket_stack.pop();

				// Check if the type of opening and closing brackets match
				if ((token == ")" && top != '(')){
					rat_throw_line("mismatched square brackets");
				}

				if ((token == "]" && top != '[')){
					rat_throw_line("mismatched round brackets");
				}
			}
		}

		// Check if there are any unmatched opening brackets left
		if (!bracket_stack.empty()) {
			rat_throw_line("unmatched opening bracket");
		}
	}

	// convert a string into a list of tokens
	std::list<std::string> Parser::tokenize(const std::string& expression)const{
		// Regular expression to match combined number and unit tokens, operators, brackets, and trigonometric functions
		// std::regex tokenRegex("(sin|cos|tan|\\d*\\.?\\d+[a-zA-Z]*|\\^|\\+|\\-|\\*|\\/|\\(|\\))");
		// const std::regex token_regex("([a-zA-Z]+|\\d*\\.?\\d+[a-zA-Z]*|\\^|\\+|\\-|\\*|\\/|\\(|\\))");
		// const std::regex token_regex("([a-zA-Z]+\\d*|\\d*\\.?\\d+[a-zA-Z]*|\\^|\\+|\\-|\\*|\\/|\\(|\\)|\\%)");
		// const std::regex token_regex("(\\d+\\.?\\d*|[a-zA-Z]+|\\^|\\+|\\-|\\*|\\/|\\(|\\)|\\[|\\]|\\%)");
		// const std::regex token_regex("(\\d+\\.?\\d*(e[-+]?\\d+)?|[a-zA-Z]+|\\^|\\+|\\-|\\*|\\/|\\(|\\)|\\[|\\]|\\%|\\==|\\!=|\\,)");
		// const std::regex token_regex("(\\d*\\.?\\d+(e[-+]?\\d+)?|[a-zA-Z]+|\\+|\\-|\\*|\\/|\\(|\\)|\\[|\\]|\\%|\\==|\\!=|\\>=|\\<=)|\\^|\\%|\\/|\\/|\\,|\\;|\\>|\\<");
		const std::regex token_regex("(\\d*\\.?\\d+(e[-+]?\\d+)?|[a-zA-Z]+|\\^|\\+|\\-|\\*|\\/|\\(|\\)|\\[|\\]|\\%|\\==|\\!=|\\,|\\;|\\>=|\\<=|\\>|\\<)");
		std::sregex_iterator begin(expression.begin(), expression.end(), token_regex);
		std::sregex_iterator end;
		
		// allocate list of tokens
		std::list<std::string> tokens;

		// keep track of previous token
		bool may_be_unary = true; // the next token may be a negation sign
		bool requires_operator = false; // an operator is expected now, if it doesn't come insert *
		bool allow_combine = false; // allow combine with previous token

		// walk over proposed tokens
		for (std::sregex_iterator i = begin; i != end; ++i) {
			// get match
			std::smatch match = *i;
			std::string token = match.str();

			// classify
			const bool is_nm = is_number(token);
			const bool is_op = is_operator(token);
			const bool is_fn = is_function(token);
			const bool is_br_op = is_bracket_open(token);
			const bool is_br_cl = is_bracket_close(token);
			const bool is_br = is_br_op || is_br_cl; 
			const bool is_var = is_variable(token);
			const bool is_unt = !is_var && !is_op && !is_fn && !is_br && !is_nm;
			const bool is_comma = token=="," || token==";";
		
			// check if added *
			bool combined = false;
			if(requires_operator){
				// must be unit
				if(is_unt){
					tokens.back() += token;
				}else if(is_fn || is_var || token=="("){
					tokens.push_back("*");
					tokens.push_back(token);
				}else if(is_fn || is_var || token=="["){
					tokens.push_back("$");
					tokens.push_back(token);
				}else{
					tokens.push_back(token);
				}
			}else{
				if(token=="-" && may_be_unary){
					tokens.push_back("neg"); // Represent unary negation with "neg"
				}else{
					if(allow_combine && is_nm && !is_comma){
						tokens.back() += token;
						combined = true;
					}else{
						tokens.push_back(token);
					}
				}
			}

			// check if bracket
			requires_operator = (is_nm || is_br_cl || is_unt || is_var) && !combined;

			// concatenate for example to make token "log10"
			allow_combine = is_unt || is_fn;

			// Update mayBeUnary for the next token
			may_be_unary = is_br_op || is_op || is_fn;
		}

		return tokens;
	} 

	// order a list of tokens into reverse polish notation
	std::list<std::string> Parser::infix_to_rpn(
		const std::list<std::string>& tokens)const{

		// create stacks
		std::stack<std::string> operator_stack;
		std::list<std::string> output_queue;
		std::stack<char> bracket_stack;

		// walk over tokens and evaluate
		for (const auto& token : tokens){
			// numerical values
			if(is_number(token)) {
				output_queue.push_back(token);
			}

			// functions
			else if(is_function(token)) {
				operator_stack.push(token);
			}

			// opening bracket
			else if(is_bracket_open(token)){
				operator_stack.push(token);
				bracket_stack.push(token[0]);
			}

			// closing bracket
			else if(is_bracket_close(token)) {
				// check if there was any opening bracket
				if (bracket_stack.empty()){
					rat_throw_line("unmatched closing bracket");
				}

				// Check for a matching opening bracket
				char top = bracket_stack.top();
				bracket_stack.pop();
				if ((token == ")" && top != '('))
					rat_throw_line("mismatched square brackets");
				if ((token == "]" && top != '['))
					rat_throw_line("mismatched round brackets");
				
				// apply oeprators with higher precedence
				while(!operator_stack.empty() && !is_bracket_open(operator_stack.top())) {
					output_queue.push_back(operator_stack.top());
					operator_stack.pop();
				}

				// pop the opening bracket
				if (!operator_stack.empty()) {
					operator_stack.pop(); // Pop the '('
					if (!operator_stack.empty() && is_function(operator_stack.top())) {
						output_queue.push_back(operator_stack.top());
						operator_stack.pop();
					}
				}

			}

			// operators
			else if(is_operator(token)){ // An operator
				// apply operators with higher precedence
				while (!operator_stack.empty() && get_precedence(operator_stack.top()) >= get_precedence(token)) {
					output_queue.push_back(operator_stack.top());
					operator_stack.pop();
				}

				// is a function argument separator
				if(!bracket_stack.empty()){
					if(bracket_stack.top()=='(' && token==","){
						operator_stack.push(",,");
					}else if(bracket_stack.top()=='(' && token==";"){
						rat_throw_line("use comma for function argument separator");
					}else{
						operator_stack.push(token);
					}
				}

				// outside brackets can not be a comma
				else if(token==","){
					rat_throw_line("can not have comma without brackets");
				}

				else if(token==";"){
					break;
				}

				// other token outside brackets
				else operator_stack.push(token);
			}

			// variables
			else if(is_variable(token)){
				output_queue.push_back(token);
			}

			// units without value
			else{
				output_queue.push_back(token);
			}
		}

		while(!operator_stack.empty()){
			output_queue.push_back(operator_stack.top());
			operator_stack.pop();
		}

		if(!bracket_stack.empty()){
			rat_throw_line("unmatched opening bracket")
		}

		return output_queue;
	}

	// evaluate reverse polish notation
	arma::field<Value> Parser::evaluate_rpn(const std::list<std::string>& rpn)const{
		// stack
		std::stack<arma::field<Value> > eval_stack;
		arma::uword num_args = 1;

		// process tokens
		for (const auto& token : rpn){
			// function
			if(is_function(token)){ // Token is a function
				// get operand
				if(eval_stack.size()<1)
					rat_throw_line("no operands for: " + token);
				
				// gather input arguments
				arma::field<arma::field<Value> > args(num_args);
				for(arma::uword i=0;i<num_args;i++){
					args(num_args-i-1) = eval_stack.top();
					eval_stack.pop();
				}

				// apply function
				eval_stack.push(fnmap_.at(token)(args));

				// reset number of arguments
				num_args = 1;
			}

			// function combinator
			else if(token==",,"){
				num_args++;
			}

			// operator
			else if(is_operator(token)){ // Token is an operator
				// check arguments
				if(num_args!=1)rat_throw_line("operator can not apply to function arguments");

				// check if there are sufficient operands on the stack
				if(eval_stack.size()<2)rat_throw_line("no operands for: " + token);
				
				// get operands
				arma::field<Value> operand2 = eval_stack.top(); 
				eval_stack.pop();
				arma::field<Value> operand1 = eval_stack.top(); 
				eval_stack.pop();

				// apply operators
				eval_stack.push(operators_.at(token)(operand1, operand2));
			}

			// variable
			else if(is_variable(token)){
				try{
					eval_stack.push(evaluate(variables_.at(token)));
				}catch(const rat_error& err){
					rat_throw_line("error during evaluating " + token + ":" + std::string(err.what()));
				}
			}

			// unit or value
			else{
				eval_stack.push({construct_value(token)});
			}
		}

		// // check scalar output
		// if(eval_stack.top().size()>1)rat_throw_line("non-scalar output");

		// convert to scalar and return
		return eval_stack.top();
	}

	// convert reverse polish notation to AST
	ShASTNodePr Parser::rpn_to_ast(const std::list<std::string>& rpn)const{
		// treenode
		std::stack<ShASTNodePr> stack;

		// walk over tokens
		for (const auto& token : rpn){
			// token is operator
			if(is_operator(token)){
				// check if number of operands left is sufficient
				if(stack.size()<2)rat_throw_line("insufficient operands for operator");

				// get left and right operands
				const ShASTNodePr right = stack.top(); // right child
				stack.pop();
				const ShASTNodePr left = stack.top(); // left child
				stack.pop();

				// create operation node
				stack.push(ASTOpNode::create(token,left,right));
			}

			// token is function
			else if(is_function(token)) {
				 // check if number of operands left is sufficient
				if(stack.empty())rat_throw_line("insufficient operands for function");
				
				// get operand
				const ShASTNodePr operand = stack.top(); // only left child for a function
				stack.pop();

				// create function node
				stack.push(ASTFnNode::create(token,operand));
			}

			else{
				// Operand
				stack.push(ASTNode::create(token));
			}
		}

		if(stack.size() != 1) {
			rat_throw_line("rpn expression is unbalanced");
		}

		return stack.top();
	}


	// convert reverse polish notation back to a normal equation
	std::string Parser::rpn_to_infix(const std::list<std::string>& rpn)const{

		// stack
		std::stack<std::pair<std::string,int> > eval_stack;

		// process tokens
		for (const auto& token : rpn){
			// function
			if(is_function(token)){
				// check stack
				if(eval_stack.size()<1)rat_throw_line("insufficient operands for this function");

				// Pop function arguments from the stack
				std::pair<std::string,int> operand = eval_stack.top();
				eval_stack.pop();

				// Build the function expression
				const std::string function_expression = (token=="neg" ? "-" : token) + "(" + operand.first + ")";
				
				// Push the function expression back onto the stack
				eval_stack.push({function_expression,100000});
			}

			// operator
			else if(is_operator(token)){

				// check stack
				if(eval_stack.size()<2)rat_throw_line("insufficient operands for this operation");

				// Pop operands from the stack
				std::pair<std::string,int> operand2 = eval_stack.top();
				eval_stack.pop();
				std::pair<std::string,int> operand1 = eval_stack.top();
				eval_stack.pop();

				// precedence
				int current_precedence = get_precedence(token);
				
				 // Check if parentheses are needed for the left expression
				if (operand1.second<current_precedence)
					operand1.first = "(" + operand1.first + ")";

				// Check if parentheses are needed for the right expression
				if (operand2.second<current_precedence || (operand2.second == current_precedence && !is_associative(token)))
					operand2.first = "(" + operand2.first + ")";

				// apply token and add brackets if needed
				std::string operator_expression = operand1.first + token + operand2.first;
				
				// // check tokens
				// const std::list<std::string> tokens = tokenize(operator_expression);
				// bool allow_simplify = true;
				// for(const auto& token2 : tokens)
				// 	if(is_variable(token2)){allow_simplify = false; break;}
				// if(allow_simplify){
				// 	const arma::field<Value> values = evaluate_rpn(infix_to_rpn(tokens));
				// 	if(values.n_elem>1)rat_throw_line("should evaluate to single value");
				// 	if(values(0).get_number()==1.0){operator_expression = "(" + values(0).get_unit().as_string() + ")"; current_precedence=10000;}
				// 	else if(values(0).get_number()==0.0){operator_expression = "0.0"; current_precedence=10000;}
				// 	else {operator_expression = std::to_string(values(0).get_number()) + "[" + values(0).get_unit().as_string() + "]";current_precedence = get_precedence("$");}
				// }

				// Push the operator expression back onto the stack
				eval_stack.push({operator_expression,current_precedence});
			}

			// unit or value
			else{
				eval_stack.push({token,100000});
			}
		}

		if(eval_stack.size()!=1)
			rat_throw_line("expression did not evaluate to a scalar");

		// convert to scalar and return
		return eval_stack.top().first;
	}


	// check wether a token is a number
	bool Parser::is_number(const std::string& token){
		return !token.empty() && (std::isdigit(token[0]) || token[0] == '.');
	}

	// check whether a token is a function
	bool Parser::is_function(const std::string& token)const{
		return fnmap_.find(token)!=fnmap_.end();
	}

	// check whether a token is an operator
	bool Parser::is_operator(const std::string& token)const{
		return operators_.find(token)!=operators_.end();
	}

	// check whether a token is a variable
	bool Parser::is_variable(const std::string& token)const{
		return variables_.find(token)!=variables_.end();
	}

	// check whether a token is a bracket open
	bool Parser::is_bracket_open(const std::string& token){
		return token=="(" || token=="[";
	}

	// check whether a token is a bracket close
	bool Parser::is_bracket_close(const std::string& token){
		return token==")" || token=="]";
	}

	// check whether a token is a bracket
	bool Parser::is_bracket(const std::string& token){
		return is_bracket_open(token) || is_bracket_close(token);
	}

	// no difference between left and right operands
	bool Parser::is_associative(const std::string& token){
		return token == "+" || token == "*";
	}


	// check the evaluation order
	int Parser::get_precedence(const std::string& token)const{
		if(token ==",,")return 1;
		if (token == ";")return 2;
		else if (token == ",")return 3;
		else if(token == "==" || token == "!=" || token==">" || token=="<" || token==">=" || token=="<=")return 4;
		else if(token == "+" || token == "-")return 5;
		else if(token == "neg")return 6; // needs less precedence than regular functions
		else if (token == "*" || token == "/" || token == "%")return 7;
		else if (token == "^")return 8;
		else if (is_function(token))return 9;
		else if (token == "$")return 10;
		return -1;
	}

}}
