// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "value.hh"

// general headers
#include <iomanip>

// rat common headers
#include "rat/common/error.hh"

// code specific to Rat
namespace rat{namespace math{

	// default constructor
	Value::Value(){
		n_ = RAT_CONST(0.0);
		u_ = Unit::D;
	} 

	// constructor from float value
	Value::Value(const fltp n, const Unit& u){
		n_ = n; u_ = u;
	}
	
	// constructor from integer value
	Value::Value(const int n, const Unit& u){
		n_ = fltp(n); u_ = u;
	}

	// float assignment operator
	void Value::operator=(const fltp n){
		n_ = n;
	}

	// equal to operator
	bool Value::operator==(const Value& r) const {
		return (u_==r.u_) && std::abs(n_-r.n_)<1e-9;
	}

	// not equal to operator
	bool Value::operator!=(const Value& r) const {
		return !(*this == r);
	}

	// larger than operator
	bool Value::operator>(const Value& r) const {
		if(u_ != r.u_)rat_throw_line("units must match to allow comparison");
		return n_>r.n_;
	}

	// smaller than operator
	bool Value::operator<(const Value& r) const {
		if(u_ != r.u_)rat_throw_line("units must match to allow comparison");
		return n_<r.n_;
	}

	// larger than or equal operator
	bool Value::operator>=(const Value& r) const {
		if(u_ != r.u_)rat_throw_line("units must match to allow comparison");
		return (*this == r) || (*this > r);
	}

	// smaller than or equal operator
	bool Value::operator<=(const Value& r) const {
		if(u_ != r.u_)rat_throw_line("units must match to allow comparison");
		return (*this == r) || (*this < r);
	}

	// addition operator
	Value Value::operator+(const Value& r) const {
		// special cases
		if(is_zero())return r;
		if(r.is_zero())return *this;
		
		// check units
		if(u_!=r.u_)rat_throw_line("Unit mismatch in operator '+' units are [" + u_.as_string() + "] and [" + r.get_unit().as_string() + "]");
		
		// return value
		return Value(n_ + r.n_, u_);
	}

	// subtraction operator
	Value Value::operator-(const Value& r) const {
		// special cases
		if(is_zero())return Value(-r.get_number(), r.get_unit());
		if(r.is_zero())return *this;

		// check units
		if(u_ != r.u_)rat_throw_line("Unit mismatch in operator '-' units are [" + u_.as_string() + "] and [" + r.get_unit().as_string() + "]");
		
		// return value
		return Value(n_ - r.n_, u_);
	}

	// multiplication operator
	Value Value::operator*(const Value& r) const {
		return Value(n_ * r.n_, u_ * r.u_);
	}

	// division operator
	Value Value::operator/(const Value& r) const {
		return Value(n_ / r.n_, u_ / r.u_);
	}

	// // modulus operator
	// Value Value::operator%(const Value& r) const {
	// 	if (u_!=r.u_)rat_throw_line("Unit mismatch in operator %");
	// 	return Value(n_ - (int)(n_ / r.n_) * r.n_, u_);
	// }

	// exponent operator
	Value Value::operator^(const Value& r) const {
		if (r.u_!=Unit::D)rat_throw_line("Non-Dimensionless Exponent: " + r.u_.as_string());
		return Value(std::pow(n_, r.n_), u_^r.n_);
	}

	// Value Value::operator|(const Value& r) const {
	// 	return Value(std::floor(n_ / r.n_), u_ / r.u_);
	// }

	// get value
	fltp Value::get_number()const{
		return n_;
	}

	// get unit
	const Unit& Value::get_unit()const{
		return u_;
	}

	// check if value is zero
	bool Value::is_zero()const{
		return n_ == RAT_CONST(0.0);
	}

	// operator with output stream
	std::ostream& operator<<(std::ostream& o, const Value& v){
		return (o<<v.as_string());
	}


	// operator with output stream for multiple values
	std::ostream& operator<<(std::ostream& o, const arma::field<Value>& v){
		if(v.empty())return o;
		if(v.n_elem==1){
			(o<<v(0).as_string());
		}
		else{
			if(v.n_rows>1)o<<"\n";
			o<<"[";
			for(arma::uword i=0;i<v.n_rows;i++){
				for(arma::uword j=0;j<v.n_cols;j++){
					o<<v(i,j).as_string();
					if(j!=v.n_cols-1)o<<", ";
				}
				if(i!=v.n_rows-1)o<<"\n";
			}
			o<<"]";
		}

		return o;
	}

	// combine into a string
	std::string Value::as_string()const{
	
		// if it is zero
		if(n_==0)return "0.0";
		
		// // exponent
		// const int exponent = static_cast<int>(floor(log10(fabs(n_))));
		// const fltp mantissa = n_ / std::pow(10, exponent);

		// output 
		std::stringstream ss; ss<<n_;
		// ss<<std::fixed<<std::setprecision(4);

		// // add exponent
		// if(exponent<-3 || exponent>3){
		// 	ss<<mantissa<<" x 10^"<<exponent;
		// }else{
		// 	ss<<n_;
		// }

		// add unit
		const std::string unit_string = u_.as_string();
		if(!unit_string.empty())ss<<" ["<<unit_string<<"]";

		// return string
		return ss.str();
	}

	bool operator==(const arma::field<Value>& a, const arma::field<Value>& b){
		if(a.n_rows!=b.n_rows || a.n_cols!=b.n_cols)return false;
		for(arma::uword i=0;i<a.n_elem;i++)if(a(i)!=b(i))return false;
		return true;
	}

	bool operator!=(const arma::field<Value>& a, const arma::field<Value>& b){
		return !(a==b);
	}

	arma::field<Value> operator-(const arma::field<Value>& a, const arma::field<Value>& b){
		if(a.n_rows!=b.n_rows || a.n_cols!=b.n_cols)
			rat_throw_line("a and b must be the same size");
		arma::field<Value> c(a.n_rows,a.n_cols);
		for(arma::uword i=0;i<a.n_elem;i++)
			c(i) = a(i)-b(i);
		return c;
	}

}}