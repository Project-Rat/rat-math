// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MATH_PARSER_HH
#define MATH_PARSER_HH

// general headers
#include <string>
#include <list>
#include <map>

// rat common headers
#include "value.hh"
#include "astnode.hh"
#include "equation.hh"

// rat specific
namespace rat{namespace math{

	// shared pointer definition
	typedef std::shared_ptr<class Parser> ShParserPr;

	// main parser class
	class Parser {
		// properties
		private:
			// map containing unit table
			std::map<std::string, Value> unit_table_;

			// map containing functions
			std::map<std::string, std::function<arma::field<Value>(const arma::field<arma::field<Value> >&)> > fnmap_;

			// map containing operators
			std::map<std::string, std::function<arma::field<Value>(const arma::field<Value>&, const arma::field<Value>&)> > operators_;

			// map containing global variables
			std::map<std::string, Equation> variables_; 

		// methods
		public:
			// constructor
			Parser();

			// factory
			static ShParserPr create();

			// load default functions
			void load_default_functions();
			void load_default_unit_table();
			void load_default_operators();

			// add variables
			void set_variable(
				const std::string& var, 
				const Equation& eq);

			// calculate derivative
			Equation calc_derivative(
				const Equation& eq, 
				const std::string& variable)const;
			std::string calc_derivative(
				const std::string& expression, 
				const std::string& variable)const;

			// construct value from a token
			Value construct_value(const std::string& token)const;

			// helper function
			static void check_brackets(
				const std::list<std::string>& tokens);
			std::list<std::string> tokenize(
				const std::string& expression)const;

			// conversion between infix and reverse polish notation
			std::list<std::string> infix_to_rpn(
				const std::list<std::string>& tokens)const;
			std::string rpn_to_infix(
				const std::list<std::string>& rpn)const;

			// function evaluation
			arma::field<Value> evaluate_rpn(
				const std::list<std::string>& rpn)const;
			
			// derivative calculation
			ShASTNodePr rpn_to_ast(
				const std::list<std::string>& rpn)const;

			// evaluation
			arma::Mat<fltp> evaluate(
				const std::string& expression, 
				const Unit& unit)const;
			arma::field<Value> evaluate(
				const std::string& expression)const;
			Value evaluate_to_scalar(
				const std::string& expression)const;

			// operations on the equation class
			arma::Mat<fltp> evaluate(
				const Equation& eq, const Unit& unit)const;
			arma::field<Value> evaluate(
				const Equation& eq)const;
			Value evaluate_to_scalar(
				const Equation& eq)const;
			fltp evaluate_to_scalar(
				const Equation& eq, 
				const Unit& unit)const;

			// Helper functions
			void check_conflicts()const;
			static bool is_number(
				const std::string& token);
			static bool is_bracket_open(
				const std::string& token);
			static bool is_bracket_close(
				const std::string& token);
			static bool is_bracket(
				const std::string& token);
			bool is_function(
				const std::string& token)const;
			bool is_operator(
				const std::string& token)const;
			bool is_variable(
				const std::string& token)const;
			static bool is_associative(
				const std::string& token);
			int get_precedence(
				const std::string& token)const;

	};

}}

#endif
