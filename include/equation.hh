// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MATH_EQUATION_HH
#define MATH_EQUATION_HH

// general headers
#include <memory>
#include <string>
#include <list>
#include <map>
#include <json/json.h>

// rat common headers
#include "value.hh"
// #include "parser.hh"

// rat specific code
namespace rat{namespace math{

	// shared pointer definition
	typedef std::shared_ptr<class Equation> ShEquationPr;

	// main equation class
	class Equation{
		// properties
		private:
			// user had a chance to assign a unit
			bool unit_assigned_ = false;

			// expression
			std::string expression_ = "0.0";

		// methods
		public:
			// constructor
			Equation();
			Equation(const Equation& eq);
			Equation(const fltp value);
			Equation(const std::string& expression);
			Equation(const char* expression);

			// assignment from other equation
			void operator=(const Equation& eq);

			// expression
			const std::string& get_expression()const;

			// // get derivative
			// Equation calc_derivative(const std::string& towards, const ShParserPr& parser)const;

			// // evaluation
			// Value evaluate_scalar(const ShParserPr& parser = Parser::create())const;
			// fltp evaluate_scalar(const Unit& expected_unit, const ShParserPr& parser = Parser::create())const;

			// // evaluate using callable operator
			// Value operator()(const ShParserPr& parser)const;
			// Value operator()(const Unit& expected_unit, const ShParserPr& parser)const;

			// assignment from json
			void operator=(const Json::Value& js);
			
			// assignment to json
			operator Json::Value() const{
				return Json::Value(std::string(expression_));
			}
	};

	// value to output stream
	std::ostream& operator<<(std::ostream& o, const Equation& v);


}}

#endif
