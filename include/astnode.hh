// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MATH_ASTNODE_HH
#define MATH_ASTNODE_HH

#include <armadillo>
#include <vector>
#include <initializer_list>
#include <ostream>
#include <string>
#include <list>
#include <memory>

// rat common headers
#include "rat/common/typedefs.hh"

// code specific to Rat
namespace rat{namespace math{

	// shared pointer definition
	typedef std::shared_ptr<class ASTNode> ShASTNodePr;

	// units
	class ASTNode{
		protected:
			std::string token_;

		public:
			ASTNode();
			virtual ~ASTNode(){};
			ASTNode(const std::string& token);
			static ShASTNodePr create(
				const std::string& token);
			const std::string& get_token()const;
			virtual ShASTNodePr calculate_derivative(
				const std::string& towards_token, 
				const arma::uword depth = 0)const;
			virtual std::list<std::string> to_rpn_notation()const;
			virtual ShASTNodePr simplify(
				const arma::uword depth = 0)const;
			virtual bool is_equal(const ShASTNodePr& other) const;
			bool is_zero()const;
			bool is_one()const;
			bool is_number()const;
			virtual std::string as_string()const;
	};

	// shared pointer definition
	typedef std::shared_ptr<class ASTOpNode> ShASTOpNodePr;

	// node for operator
	class ASTOpNode: public ASTNode{
		protected:
			ShASTNodePr left_;
			ShASTNodePr right_;

		public:
			ASTOpNode(
				const std::string& token, 
				const ShASTNodePr& left, 
				const ShASTNodePr& right);
			static ShASTOpNodePr create(
				const std::string& token, 
				const ShASTNodePr& left, 
				const ShASTNodePr& right);
			virtual ShASTNodePr calculate_derivative(
				const std::string& towards_token, 
				const arma::uword depth = 0)const override;
			virtual std::list<std::string> to_rpn_notation()const override;
			virtual ShASTNodePr simplify(
				const arma::uword depth = 0)const override;
			virtual bool is_equal(const ShASTNodePr& other) const override;
			static ShASTNodePr simplify(
				const ShASTNodePr& simplify_left, 
				const std::string& token, 
				const ShASTNodePr& simplify_right);
			virtual std::string as_string()const override;
	};

	// shared pointer definition
	typedef std::shared_ptr<class ASTFnNode> ShASTFnNodePr;
	
	// node for function
	class ASTFnNode: public ASTNode{
		protected:
			ShASTNodePr operand_;

		public:
			ASTFnNode(
				const std::string& token, 
				const ShASTNodePr& operand);
			static ShASTFnNodePr create(
				const std::string& token, 
				const ShASTNodePr& operand);
			const ShASTNodePr& get_operand()const;
			virtual ShASTNodePr calculate_derivative(
				const std::string& towards_token, 
				const arma::uword depth = 0)const override;
			virtual std::list<std::string> to_rpn_notation()const override;
			virtual ShASTNodePr simplify(
				const arma::uword depth = 0)const override;
			virtual bool is_equal(const ShASTNodePr& other) const override;
			virtual std::string as_string()const override;


	};

}}

#endif
