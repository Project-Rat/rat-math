// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MATH_UNIT_HH
#define MATH_UNIT_HH

#include <armadillo>
#include <vector>
#include <initializer_list>
#include <ostream>
#include <string>

// rat common headers
#include "rat/common/typedefs.hh"

// code specific to Rat
namespace rat{namespace math{

	// units
	class Unit{
		private:
			// stored dimensions
			arma::Row<fltp> dim_;


		public:
			// constructors
			Unit();
			Unit(const arma::Row<fltp>& dim);

			// operators
			bool operator==(const Unit& r) const;
			bool operator!=(const Unit& r) const;
			Unit operator+(const Unit& r) const;
			Unit operator-(const Unit& r) const;
			Unit operator/(const Unit& r) const;
			Unit operator*(const Unit& r) const;
			Unit operator%(const Unit& r) const;
			Unit operator^(const fltp f) const;
			Unit operator^(const arma::uword f) const;
			Unit operator^(const int f) const;

			// get dimensions
			const arma::Row<fltp>& get_dim()const;
			arma::Row<fltp> get_ext_dim()const;

			// convert to string
			static void uprint(
				std::ostream& o, 
				const std::string& x, 
				const fltp f);
			std::string as_string()const;

			// Static Unit constants
			static const Unit D, s, m, kg, A, K, mol, cd;
	};


	void uprint(std::ostream& o, std::string x, double f);
	std::ostream& operator<<(std::ostream& o, const Unit& u);


}}

#endif
