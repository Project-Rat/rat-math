// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MATH_VALUE_HH
#define MATH_VALUE_HH

#include <armadillo>
#include <vector>
#include <initializer_list>
#include <ostream>
#include <string>

// rat math headers
#include "unit.hh"

// code specific to Rat
namespace rat{namespace math{

	class Value{
		private:
			fltp n_;
			Unit u_;

		public:
			// constructors
			Value();
			Value(const fltp n, const Unit& u = Unit::D);
			Value(const int n, const Unit& u = Unit::D);

			// assignment 
			void operator=(const fltp n);

			// (in)equality
			bool operator==(const Value& r) const;
			bool operator!=(const Value& r) const;
			
			// comparison
			bool operator>(const Value& r) const;
			bool operator<(const Value& r) const;
			bool operator>=(const Value& r) const;
			bool operator<=(const Value& r) const;

			// operators
			Value operator+(const Value& r) const;
			Value operator-(const Value& r) const;
			Value operator*(const Value& r) const;
			Value operator/(const Value& r) const;
			Value operator^(const Value& r) const;
			// Value operator%(const Value& r) const;
			// Value operator|(const Value& r) const;

			// get values
			fltp get_number()const;

			// get unit
			const Unit& get_unit()const;

			// check if value is zero
			bool is_zero()const;

			// convert to string
			std::string as_string()const;
	};

	// value to output stream
	std::ostream& operator<<(std::ostream& o, const Value& v);
	std::ostream& operator<<(std::ostream& o, const arma::field<Value>& v);

	// operations on field arrays with values
	bool operator==(const arma::field<Value>& a, const arma::field<Value>& b);
	bool operator!=(const arma::field<Value>& a, const arma::field<Value>& b);
	arma::field<Value> operator-(const arma::field<Value>& a, const arma::field<Value>& b);
}}

#endif