// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general headers
#include <armadillo>
#include <iostream>
#include <cmath>
#include <iomanip> 

// rat common headers
#include "rat/common/error.hh"

// rat math headers
#include "equation.hh"
#include "parser.hh"


// main
int main(){
	
	// build a parser
	const rat::math::ShParserPr ps = rat::math::Parser::create();

	std::cout << std::fixed << std::setprecision(16) << std::endl;

	// Variables without units
	ps->set_variable("x", "2");
	ps->set_variable("y", "4");
	if(ps->evaluate(rat::math::Equation("x + y")) != 
		ps->evaluate(rat::math::Equation("6")))
	    rat_throw_line("Addition with variables did not work");
	if(ps->evaluate(rat::math::Equation("x - y")) != 
		ps->evaluate(rat::math::Equation("-2")))
	    rat_throw_line("Subtraction with variables did not work");
	if(ps->evaluate(rat::math::Equation("x * y")) != 
		ps->evaluate(rat::math::Equation("8")))
	    rat_throw_line("Multiplication with variables did not work");
	if(ps->evaluate(rat::math::Equation("x / y")) != 
		ps->evaluate(rat::math::Equation("0.5")))
	    rat_throw_line("Division with variables did not work");
	if(ps->evaluate(rat::math::Equation("x^y")) != 
		ps->evaluate(rat::math::Equation("16")))
	    rat_throw_line("Exponentiation with variables did not work");
	if(ps->evaluate(rat::math::Equation("(x + y) * (x - y)")) != 
		ps->evaluate(rat::math::Equation("-12")))
	    rat_throw_line("Complex expression involving variables did not work");

	// variables with units
	ps->set_variable("x", "2 [m/s]");
	ps->set_variable("y", "10 [m]");
	if(ps->evaluate(rat::math::Equation("y/x")) != 
		ps->evaluate(rat::math::Equation("5[s]")))
	    rat_throw_line("Variables with units does not work");


	// return
	return 0;
}
