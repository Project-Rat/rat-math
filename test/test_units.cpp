// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general headers
#include <armadillo>
#include <iostream>
#include <cmath>
#include <iomanip> 

// rat common headers
#include "rat/common/error.hh"

// rat math headers
#include "equation.hh"
#include "parser.hh"


// main
int main(){
	
	// build a parser
	const rat::math::ShParserPr ps = rat::math::Parser::create();

	std::cout << std::fixed << std::setprecision(16) << std::endl;

	// Equations with Units
	// Addition with Units
	if(ps->evaluate(rat::math::Equation("2[m] + 3[m]")) != 
		ps->evaluate(rat::math::Equation("5[m]")))
	    rat_throw_line("Addition with units did not work");

	// Subtraction with Units
	if(ps->evaluate(rat::math::Equation("5[m] - 2[m]")) != 
		ps->evaluate(rat::math::Equation("3[m]")))
	    rat_throw_line("Subtraction with units did not work");

	// Multiplication with a Number and a Unit
	if(ps->evaluate(rat::math::Equation("3 * 2[m]")) != 
		ps->evaluate(rat::math::Equation("6[m]")))
	    rat_throw_line("Multiplication with a number and a unit did not work");

	// Division with Units
	if(ps->evaluate(rat::math::Equation("10[m] / 2")) != 
		ps->evaluate(rat::math::Equation("5[m]")))
	    rat_throw_line("Division with units did not work");

	// Complex Units (Division of Units)
	if(ps->evaluate(rat::math::Equation("10[m/s]")) != 
		ps->evaluate(rat::math::Equation("10[m/s]")))
	    rat_throw_line("Complex units (division of units) did not work");

	// Logarithms
	if(ps->evaluate(rat::math::Equation("10[m]log10(10)")) != 
		ps->evaluate(rat::math::Equation("10[m]")))
	    rat_throw_line("Logarithm did not work");

	// return
	return 0;
}
