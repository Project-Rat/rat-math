// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general headers
#include <armadillo>
#include <iostream>
#include <cmath>
#include <iomanip> 

// rat common headers
#include "rat/common/error.hh"

// rat math headers
#include "equation.hh"
#include "parser.hh"


// main
int main(){
	
	// build a parser
	const rat::math::ShParserPr ps = rat::math::Parser::create();

	std::cout << std::fixed << std::setprecision(16) << std::endl;

	// Simple Equations
	// Addition of Numbers
	if(ps->evaluate(rat::math::Equation("2 + 3")) != 
		ps->evaluate(rat::math::Equation("5")))
	    rat_throw_line("Addition of numbers did not work");

	// Subtraction of Numbers
	if(ps->evaluate(rat::math::Equation("5 - 3")) != 
		ps->evaluate(rat::math::Equation("2")))
	    rat_throw_line("Subtraction of numbers did not work");

	// Multiplication of Numbers
	if(ps->evaluate(rat::math::Equation("4 * 3")) != 
		ps->evaluate(rat::math::Equation("12")))
	    rat_throw_line("Multiplication of numbers did not work");

	// Division of Numbers
	if(ps->evaluate(rat::math::Equation("10 / 2")) != 
		ps->evaluate(rat::math::Equation("5")))
	    rat_throw_line("Division of numbers did not work");

	// Exponentiation
	if(ps->evaluate(rat::math::Equation("2 ^ 3")) != 
		ps->evaluate(rat::math::Equation("8")))
	    rat_throw_line("Exponentiation did not work");

	// return
	return 0;
}
