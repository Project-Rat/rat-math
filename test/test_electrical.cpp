// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general headers
#include <armadillo>
#include <iostream>
#include <cmath>
#include <iomanip> 

// rat common headers
#include "rat/common/error.hh"

// rat math headers
#include "equation.hh"
#include "parser.hh"


// main
int main(){
	
	// build a parser
	const rat::math::ShParserPr ps = rat::math::Parser::create();

	std::cout << std::fixed << std::setprecision(16) << std::endl;

	// Electrical equations
	// Ohms Law (RI = V)
	ps->set_variable("current", "2 [A]");
	ps->set_variable("resistance", "5 [Ohm]");
	if(ps->evaluate(rat::math::Equation("current*resistance")) != 
		ps->evaluate(rat::math::Equation("10[V]")))
	    rat_throw_line("Ohms Law did not work");

	// Capacitive Reactance (Xc = 1 / (2 * pi * f * C))
	ps->set_variable("frequency", "50 [Hz]"); // Frequency in Hertz
	ps->set_variable("capacitance", "0.001 [F]"); // Capacitance in Farads
	if(ps->evaluate(rat::math::Equation("1/(2 * pi * frequency * capacitance)")) != 
		ps->evaluate(rat::math::Equation("3.183098861837907[Ohm]")))
	    rat_throw_line("Capacitive Reactance calculation did not work");

	// Inductive Reactance (Xl = 2 * pi * f * L)
	ps->set_variable("frequency", "60 [Hz]"); // Frequency in Hertz
	ps->set_variable("inductance", "0.002 [H]"); // Inductance in Henries
	if(ps->evaluate(rat::math::Equation("2 * pi * frequency * inductance")) != 
		ps->evaluate(rat::math::Equation("0.7539822368615503[Ohm]")))
	    rat_throw_line("Inductive Reactance calculation did not work");

	// Power in an Electrical Circuit (P = V * I)
	ps->set_variable("voltage", "120 [V]"); // Voltage in Volts
	ps->set_variable("current", "0.5 [A]"); // Current in Amperes
	if(ps->evaluate(rat::math::Equation("voltage * current")) != 
		ps->evaluate(rat::math::Equation("60[W]")))  // Output in Watts
	    rat_throw_line("Power calculation did not work");

	// Energy Stored in a Capacitor (E = 0.5 * C * V^2)
	ps->set_variable("capacitance", "0.0001 [F]"); // Capacitance in Farads
	ps->set_variable("voltage", "12 [V]"); // Voltage in Volts
	if(ps->evaluate(rat::math::Equation("0.5 * capacitance * voltage^2")) != 
		ps->evaluate(rat::math::Equation("7.2[mJ]"))) // Output in Joules
	    rat_throw_line("Energy stored in a capacitor did not work");

	// Energy Stored in an Inductor (E = 0.5 * L * I^2)
	ps->set_variable("inductance", "0.05 [H]"); // Inductance in Henries
	ps->set_variable("current", "3 [A]"); // Current in Amperes
	if(ps->evaluate(rat::math::Equation("0.5 * inductance * current^2")) != 
		ps->evaluate(rat::math::Equation("0.225[J]"))) // Output in Joules
	    rat_throw_line("Energy stored in an inductor did not work");

	// Charge in a Capacitor (Q = C * V)
	ps->set_variable("capacitance", "0.0005 [F]"); // Capacitance in Farads
	ps->set_variable("voltage", "10 [V]"); // Voltage in Volts
	if(ps->evaluate(rat::math::Equation("capacitance * voltage")) != 
		ps->evaluate(rat::math::Equation("0.005[C]"))) // Output in Coulombs
	    rat_throw_line("Charge in a capacitor did not work");

	// return
	return 0;
}
