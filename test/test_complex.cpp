// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general headers
#include <armadillo>
#include <iostream>
#include <cmath>
#include <iomanip> 

// rat common headers
#include "rat/common/error.hh"

// rat math headers
#include "equation.hh"
#include "parser.hh"


// main
int main(){
	
	// build a parser
	const rat::math::ShParserPr ps = rat::math::Parser::create();

	std::cout << std::fixed << std::setprecision(16) << std::endl;

	// Complex Equations
	// Combination of Operations
	if(ps->evaluate(rat::math::Equation("2 + 3 * 4 - 5")) != 
		ps->evaluate(rat::math::Equation("9")))
	    rat_throw_line("Combination of operations did not work");

	// Equation with Parentheses
	if(ps->evaluate(rat::math::Equation("(2 + 3) * (4 - 5)")) != 
		ps->evaluate(rat::math::Equation("-5")))
	    rat_throw_line("Equation with parentheses did not work");

	// Equation with Functions and Numbers
	if(ps->evaluate(rat::math::Equation("sin(pi*rad/2) + log(exp(1))")) != 
		ps->evaluate(rat::math::Equation("2")))
	    rat_throw_line("Equation with functions and numbers did not work");

	// Equation with Units and Functions
	if(ps->evaluate(rat::math::Equation("2[m/s] * cos(0rad) + 10[m/s] / 2")) != 
		ps->evaluate(rat::math::Equation("7[m/s]")))
	    rat_throw_line("Equation with units and functions did not work");

	// Complex Nested Functions
	if(ps->evaluate(rat::math::Equation("sqrt(16) + exp(log(4)) - 3^2")) != 
		ps->evaluate(rat::math::Equation("-1")))
	    rat_throw_line("Complex nested functions did not work");

	// More Complex Units
	// Multiplication and Division with Units
	if(ps->evaluate(rat::math::Equation("2[m^2] * 3[m^-2] / 6")) != 
		ps->evaluate(rat::math::Equation("1")))
	    rat_throw_line("Multiplication and division with units did not work");

	// Unit Conversion (assuming the calculator can handle unit conversions)
	if(ps->evaluate(rat::math::Equation("1[km] / 1[m]")) != 
		ps->evaluate(rat::math::Equation("1000")))
	    rat_throw_line("Unit conversion did not work");


	// return
	return 0;
}
