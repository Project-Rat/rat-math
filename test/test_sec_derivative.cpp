// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// general headers
#include <armadillo>
#include <iostream>
#include <cmath>
#include <iomanip> 

// rat common headers
#include "rat/common/error.hh"

// rat math headers
#include "equation.hh"
#include "parser.hh"


// main
int main(){
	
	// build a parser
	const rat::math::ShParserPr parser = rat::math::Parser::create();
	parser->set_variable("time", "linspace(-pi[s],pi[s],20)");
    parser->set_variable("omega", "2.1[rad/s]");

	std::cout << std::fixed << std::setprecision(16) << std::endl;

	// test case one
	const std::string variable = "time";
	rat::math::Equation ref_eq("sec(time*omega)");
	rat::math::Equation ref_deq("omega*sec(omega*time)*tan(omega*time)");
	rat::math::Equation ref_ddeq("omega^2*sec(omega*time)*(tan(omega*time)^2+sec(omega*time)^2)");
	rat::math::Equation ref_dddeq("omega^3*sec(omega*time)*tan(omega*time)*(tan(omega*time)^2+5*sec(omega*time)^2)");
	rat::math::Equation ref_ddddeq("omega^4*sec(omega*time)*(tan(omega*time)^4+18*sec(omega*time)^2*tan(omega*time)^2+5*sec(omega*time)^4)");
	rat::math::Equation ref_dddddeq("omega^5*sec(omega*time)*tan(omega*time)*(tan(omega*time)^4+58*sec(omega*time)^2*tan(omega*time)^2+61*sec(omega*time)^4)");

	// calculate derivatives
	const rat::math::Equation deq = parser->calc_derivative(ref_eq,variable);
	const rat::math::Equation ddeq = parser->calc_derivative(deq,variable);
	const rat::math::Equation dddeq = parser->calc_derivative(ddeq,variable);
	const rat::math::Equation ddddeq = parser->calc_derivative(dddeq,variable);
	const rat::math::Equation dddddeq = parser->calc_derivative(ddddeq,variable);

	// test equations
	if(parser->evaluate(ref_deq)!=parser->evaluate(deq))
		rat_throw_line("first derivative does not evaluate to same value as reference");
	if(parser->evaluate(ref_ddeq)!=parser->evaluate(ddeq))
		rat_throw_line("second derivative does not evaluate to same value as reference");
	if(parser->evaluate(ref_dddeq)!=parser->evaluate(dddeq))
		rat_throw_line("third derivative does not evaluate to same value as reference");
	if(parser->evaluate(ref_ddddeq)!=parser->evaluate(ddddeq))
		rat_throw_line("fourth derivative does not evaluate to same value as reference");
	if(parser->evaluate(ref_dddddeq)!=parser->evaluate(dddddeq))
		rat_throw_line("fifth derivative does not evaluate to same value as reference");

	// return
	return 0;
}
