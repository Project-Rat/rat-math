// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// command line parser
#include <tclap/CmdLine.h>

// rat common headers
#include "rat/common/error.hh"
#include "rat/common/extra.hh"

// rat math headers
#include "equation.hh"
#include "version.hh"
#include "parser.hh"

int main(int argc, char * argv[]){

	// COMMAND LINE INPUT
	// create tclap object
	TCLAP::CmdLine cmd("Math expression evaluator",' ',rat::cmn::Extra::create_version_tag(
		CMN_PROJECT_VER_MAJOR,CMN_PROJECT_VER_MINOR,CMN_PROJECT_VER_PATCH));

	// filename input argument
	TCLAP::UnlabeledValueArg<std::string> string_argument(
		"str","Input string to be parsed",true,"time^3", "string", cmd);

	TCLAP::UnlabeledValueArg<std::string> variable_argument(
		"var","Variable towards which the derivative is calculated.", false, "time", "string", cmd);

	TCLAP::ValueArg<int> order_argument(
		"","order","Which derivative to calculate.", false, 0, "string", cmd);

	// parse the command line arguments
	cmd.parse(argc, argv);


	// parser
	const rat::math::ShParserPr parser = rat::math::Parser::create();
	parser->set_variable("time", "0.4[s]");
	parser->set_variable("omega", "2.1[rad/s]");
	parser->set_variable("tau", "4.0[s]");
	parser->set_variable("velocity", "0.2[m/s]");

	// create the equation
	rat::math::Equation eq(string_argument.getValue());

	// evaluate and show result
	std::cout<<0<<": "<<eq<<" evaluates to: "<<parser->evaluate(eq)<<std::endl;
	std::cout<<std::endl;
	for(arma::uword i=0;i<order_argument.getValue();i++){
		eq = parser->calc_derivative(eq, variable_argument.getValue());
		std::cout<<i+1<<": "<<eq<<" evaluates to: "<<parser->evaluate(eq)<<std::endl;
		std::cout<<std::endl;
	}


	// return 0
	return 0;
}
